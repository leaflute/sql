    -- [ 시스템 계정(System) 접속 ] ----------------------------------------------
   
   -- 2-1. 계정 생성
   -- create user <계정이름> identified by <계정암호> default tablespace users;
   create user madang identified by tiger default tablespace users;
   
   -- 비밀번호 변경
   -- alter user <계정이름> identified by <변경 비밀번호>
   alter user madang identified by tiger;
    
   -- 2-2. 사용자 권한 부여
   -- grant connect, resource, create view to <계정이름>
   grant connect, resource, create view to madang;
   -- grant create view to hr;

   -- 2-3. 락해제
   -- alter user <계정> account unlock;
   alter user madang account unlock;
   
   -- 2-4. 계정삭제
   drop user madang cascade;
   
   
   