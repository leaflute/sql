--
/*
     p226 ch09 데이터 조작과 트랜지션
    1. 테이블에 데이터를 추가하는 INSERT문
    - DML(데이터 조작어 : Data Manipulation Language)은 테이블에 
      새로운 데이터를 삽입하거나, 기존의 데이터를 수정, 삭제하기 위한 명령어의 집합이다.
    - INSERT문은 테이블에 데이터를 입력하기 위한 명령문이다.
    - 형식 : INSERT INTO table_name(column_name, ...)
             VALUES(column_value, ...); -- 컬럼 순서대로 원하는 컬럼에 값을 삽입
    - 기술한 컬럼수와 값의 갯수, 순서가 일치해야 한다.
    - INTO절에 컬럼을 명시하지 않으면, 테이블을 생성할 때 정의한 컬럼순서와 동일한 순서대로 입력된다.
    - 형식 : INSERT INTO table_name
             VALUES(column_value, ...); -- 컬럼 순서대로 원하는 컬럼에 값을 삽입
    - 한번에 하나의 로우만 INTO 뒤에 명시한 테이블에 삽입된다.
    - 데이터 타입이 문자와 날짜일 경우에는 반드시 작은따옴표('')를 사용해야 한다.
    - COMMIT : 데이터베이스에 영구적으로 데이터를 저장하는 명령문
    - ROLLBACK : INSERT, UPDATE, DELETE 문을 실행 후 취소하는 명령문(COMMIT 후에는 롤백불가)
*/
-- 1. 테이블 생성
DROP TABLE play_tbl;
CREATE TABLE play_tbl (
    play_code   NUMBER(3)       PRIMARY KEY, -- 식별자
    play_name   VARCHAR2(50)    NOT NULL,
    play_place  VARCHAR2(50)    NOT NULL,
    play_price  NUMBER(8)       DEFAULT 0,
    play_number NUMBER(5)       DEFAULT 0,
    play_date   DATE            DEFAULT sysdate
);

DESC play_tbl;

-- 2. INSERT
INSERT INTO play_tbl(play_code, play_name, play_place, play_price, play_number)
VALUES(1, '롯데월드', '잠실', 40000, 2);

INSERT INTO play_tbl(play_code, play_name, play_place, play_price, play_number)
VALUES(2, '서울타워', '남산', 50000, 2);

INSERT INTO play_tbl(play_code, play_name, play_place, play_price, play_number)
VALUES(3, '방탈출카페', '홍대', 20000, 5);

INSERT INTO play_tbl(play_code, play_name, play_place, play_price, play_number)
VALUES(4, 'VR', '신촌', 30000, 3);

INSERT INTO play_tbl(play_code, play_name, play_place, play_price, play_number)
VALUES(5, '실내 스카이다이빙', '용인', 100000, 3);


-- 3. UPDATE
UPDATE play_tbl
   SET play_date = '2021/06/20'
 WHERE play_code = 1;  
 
UPDATE play_tbl
   SET play_date = '2021/06/21', play_name = '서울타워'
 WHERE play_code = 2;   

UPDATE play_tbl
   SET play_date = '2021/06/21'
 WHERE play_code = 3; 
 
UPDATE play_tbl
   SET play_date = '2021/06/21'
 WHERE play_code = 4;  
 
UPDATE play_tbl
   SET play_date = '2021/06/23'
 WHERE play_code = 5;    

COMMIT;
SELECT * FROM play_tbl; 
 
/* 
    방법1) 데이터를 제외한 구조만 복사후 데이터를 INSERT
    CREATE TABLE 복사tbl
        AS subquery
     WHERE 0=1;
     
    2. INSERT SELECT 
*/

CREATE TABLE play_tbl_copy
    AS (SELECT * FROM play_tbl WHERE 0=1);

INSERT INTO play_tbl_copy (SELECT * FROM play_tbl);    
    
SELECT * FROM play_tbl_copy;   

/* 
    방법2) 테이블 생성 시 데이터도 같이 가지고 오기
    CREATE TABLE 복사tbl
        AS subquery;     
*/

CREATE TABLE play_tbl_copy2
    AS (SELECT * FROM play_tbl);
    
SELECT * FROM play_tbl_copy2;      

/*
    p232 2. 테이블의 내용을 수정하는 UPDATE문
    - UPDATE문은 테이블에 저장된 데이터를 수정하기위한 DML
    - 주의사항 : pk는 수정하지 말 것(DELETE 후 INSERT할 것)
    - 형식
    UPDATE 테이블명 
       SET 컬럼1 = 값1, 컬럼2 = 값2 ...
     WHERE 조건
*/
-- play code가 5번인 장소를 가디단역으로 변경하고 가격을 90000, 인원수 5명, 일자는 2021/07/01로 변경
UPDATE play_tbl
   SET play_place = '가산디지털단지역', play_price = 90000, play_number = 5, play_date = '2021/07/01'
 WHERE play_code = 5;
 
-- play code가 4번인 장소를 여의도역으로 변경하고 가격을 90000, 인원수 4명, 일자는 2021/07/03으로 변경 
UPDATE play_tbl
   SET play_place = '여의도', play_price = 40000, play_number = 10, play_date = '2021/07/03'
 WHERE play_code = 4;

COMMIT;
SELECT * FROM play_tbl; 

----
SELECT * FROM menu_tbl;
-- I : 7 빕스 US 립 50000 5
-- U : 7        스테이크 30000 4
-- I : 8 구내식당 KR 잡채밥 5500 2

INSERT INTO menu_tbl(food_code, restaurant_name, food_kind, food_name, food_price, starpoint)
VALUES (7, '빕스', 'KR', '립', 50000, 5);

INSERT INTO menu_tbl(food_code, restaurant_name, food_kind, food_name, food_price, starpoint)
VALUES (8, '구내식당', 'KR', '잡채밥', 5500, 2);

UPDATE menu_tbl
   SET food_name = '스테이크', food_price = 30000, starpoint = 4
 WHERE food_code = 7;  

COMMIT;

/*
    p233
    2-1. 다른테이블을 기반으로 데이터 수정하기
    UPDATE문의 SET절에서 서브쿼리를 기술하면, 서브쿼리를 수행한 결과로 내용이 변경
*/
SELECT * FROM menu_tbl;
-- 3번의 food_name, 5번의 food_price를 읽어서 8번의 food_name, food_price 변경
UPDATE menu_tbl
   SET food_name = (SELECT food_name FROM menu_tbl WHERE food_code = 0)
     , food_price = (SELECT food_price FROM menu_tbl WHERE food_code = 2) 
 WHERE food_code = 8;

UPDATE menu_tbl
   SET (food_name, food_price) = (SELECT food_name, food_price FROM menu_tbl WHERE food_code = 2)
 WHERE food_code = 7; 
 
/*
    p235 3. 테이블의 내용을 삭제하는 DELETE문
    - DELETE문을 사용하여 기존에 저장된 데이터를 삭제
    - WHERE 생략 시 모든 행 삭제
    - 형식
    DELETE [FROM] 테이블이름
    WHERE 조건명;
*/

SELECT * FROM tab;
SELECT * FROM menu_tbl_copy;
-- food_code가 3인 데이터 삭제
-- food_code가 4인 데이터 추가 (4, '다원', 'CN', '송이덮밥', 8000, 3, '2021/06/23')
-- food_code가 1인 데이터 수정 (1, '잡채덮밥', 6000)
-- food_name이 '덮밥'을 포함하는 데이터 삭제

DELETE FROM menu_tbl_copy
 WHERE food_code = 3;

INSERT INTO menu_tbl_copy(food_code, restaurant_name, food_kind, food_name, food_price, starpoint, start_date)
VALUES(6, '다원', 'CN', '송이덮밥', 8000, 3, '2021/06/23');

UPDATE menu_tbl_copy
   SET food_name = '잡채덮밥', food_price = 6000
 WHERE food_code = 0;
 
DELETE FROM menu_tbl_copy
 WHERE food_name LIKE '%덮밥%';

--(6) menu_tbl1_copy테이블에서 RESTAURANT_NAME이 '서브웨이'의 food_price 가격보다 큰 모든 데이터를 삭제 
DELETE FROM menu_tbl_copy 
 WHERE food_price > (SELECT food_price 
                       FROM menu_tbl_copy 
                      WHERE restaurant_name = '서브웨이');
 
ROLLBACK;
COMMIT;

/*
    p238 트랜잭션 관리
    - 트랜잭션은 데이터 처리에서 논리적인 하나의 작업 단위를 의미한다.
    - ALL or Nothing : 여러개의 명령어 집합이 정상적으로 처리되면 정상종료하고,
       명령어들중 하나라도 잘못된다면 전체를 취소한다.
    - DML 작업이 성공적으로 처리되었다면 COMMIT을, 취소해야 한다면 ROLLBACK 명령을 실행한다.
    - DDL(테이블생성), DCL(권한)문이 실행되는 경우에는 자동으로 COMMIT 된다.
    - COMMIT - 트랜잭션의 처리과정을 반영하여 변경된 내용을 영구저장한다. 모든 작업들의 정상처리확정 명령어다.
               INSERT, UPDATE, DELETE(즉 DML) 후 COMMIT을 해야 한다.
    - SAVEPOINT 세이브이름;
    - ROLLBACK TO 세이브이름;
*/
DROP TABLE dept_tr;
CREATE TABLE dept_tr(
    deptno      NUMBER(2)       PRIMARY KEY,
    deptname    VARCHAR2(50)    NOT NULL,
    loc         VARCHAR2(50)
);

INSERT INTO dept_tr(deptno, deptname, loc)
VALUES(10, 'IT', '서울');

INSERT INTO dept_tr(deptno, deptname, loc)
VALUES(20, '기획부', '인천');

INSERT INTO dept_tr(deptno, deptname, loc)
VALUES(30, '영업부', '대구');

INSERT INTO dept_tr(deptno, deptname, loc)
VALUES(40, '마케팅', '광주');

INSERT INTO dept_tr(deptno, deptname, loc)
VALUES(50, '경영지원', '경기도');

COMMIT;

SELECT * FROM dept_tr;

-- 트랜잭션
-- 1) 40번 부서 삭제 후 COMMIT
DELETE dept_tr
 WHERE deptno = 40;
 
COMMIT;

-- 2) 30번 부서 삭제 후 SAVEPOINT S1 설정
DELETE dept_tr
 WHERE deptno = 30;

SAVEPOINT S1;

-- 3) 20번 부서 삭제 후 SAVEPOINT S2 설정
DELETE dept_tr
 WHERE deptno = 20;
 
SAVEPOINT S2;

-- 4) 10번 부서 삭제
DELETE dept_tr
 WHERE deptno = 10;

-- 트랜잭션 중간 단계로 되돌리기
-- 10번 부서 삭제 취소
ROLLBACK TO S2;

-- 20번 부서 삭제 취소
ROLLBACK TO S1;

-- 30번 부서 삭제 취소
ROLLBACK;

-- 40번 부서 삭제 취소