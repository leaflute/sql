 -- ch04
/* 
    [2일차]
    ch04. 다양한 함수
    
    4.1.1 문자 함수(대소문자 변환함수)
    A. LOWER : 소문자로 변환
    B. UPPER : 대문자로 변환
    C. INICAP : 첫 글자만 대문자로, 나머지는 소문자로 변환

*/

 -- 사원테이블의 last_name이 king인 경우 가장 앞 글자에 대문자를 붙여 last_name 조회
SELECT *
  FROM employees
 WHERE last_name = INITCAP('king');
 
 -- 사원테이블의 이메일이 'TFOX'일 경우 email 조회
SELECT *
  FROM employees
 WHERE email = UPPER('tfox'); 

 -- LENGTH : 문자의 길이를 글자 수로 반환한다. (영문 1, 한글 1) 
 -- LENGTHB : 문자의 길이를 byte 수로 반환한다(한글 3Byte) : 한글 UTF-8 => 초성, 중성, 종성
SELECT LENGTH('Carpe diem') "Carpe Diem의 글자수"
  FROM dual;

SELECT LENGTHB('Seize the day') "Seize the day의 바이트수"
FROM dual;

/*
    p105
    4.1.2 문자 조작 함수(문자 처리 함수) => 중요
    CONCAT - 문자열을 연결('||'를 써도 된다)
    SUBSTR - 문자를 잘라 추출
    INSTR - 특정 문자의 위치값을 변환
    LPAD, RPAD - 입력받은 문자열과 기호를 정렬하여 특정 길이의 문자열로 반환
    TRIM - 잘라내고 남은 문자를 표시
   
*/

 --  CONCAT - 문자열을 연결
 -- '03/06/17'에 입사한 사원의 사번, 입사일(03/06/17 -> 2003/06/17), 이름(first_name + ' ' + last_name) 검색 , 별칭부여
SELECT employee_id
     , concat('20', hire_date) 입사일
     , first_name || ' ' || last_name 이름
  FROM employees
 WHERE hire_date = '03/06/17'; 
 
/*
    SUBSTR - 문자열에서 일부만 추출
    SUBSTR('문자열', 시작위치, 추출할 개수)
    양수이면 앞쪽부터, 음수이면 뒤쪽부터 추출
*/
SELECT '장군의 아들 김두한'
     , SUBSTR('장군의 아들 김두한',-3,3)
  FROM dual;

/*
    INSTR - 문자열 내에 해당 문자가 어느 위치에 존재하는지 알려줌
    - 형식 : INSTR(대상, 찾을 글자, 시작위치, 몇번째 발견)
            시작 위치와 몇번째 발견이 생략시 모두 1로 간주
*/
 -- 'Oracle mania'에서 a가 1부터 시작, 1번째 발견된 위치
SELECT INSTR('Oracle Mania', 'a', 1, 1) "오라클 매니아"
  FROM dual;
 -- 'Fly me to the moon'에서 o가 12부터 시작, 2번째 발견된 위치
SELECT INSTR('Fly me to the moon', 'o', 12, 2) "플라이 미 투더 문"
  FROM dual;
  
/*
    4.1.4 문법 RPAD, LPAD(컬럼명, 자릿수, '특정 기호')
    RPAD
    - 대상 문자열을 명시된 자릿수에서 왼쪽에 나타내고, 남은 오른쪽 자리를 특정 기호로 채움
    
    LPAD
    - 대상 문자열을 명시된 자릿수에서 오른쪽에 나타내고, 남은 왼쪽 자리를 특정 기호로 채움
*/
 -- RPAD
SELECT ' Ill be back '
     , RPAD(' Ill be back ', 15, '*')
  FROM dual;

 -- LPAD
SELECT ' alea jacta est! '
     , LPAD(' alea jacta est! ', 20,'&')
  FROM dual;
  
-- 문자열의 특정 문자를 다른 문자로 치환
-- REPLACE([문자열 데이터 또는 컬럼명], [찾는 문자], [대체할 문자(생략 가능)])
SELECT '2021-06-18' AS REPLACE_BEFORE
     , REPLACE('2021-06-18', '-', ' ') AS REPLACE_1 -- 2021 06 18
     , REPLACE('2021-06-18', '-', '/') AS REPLACE_2
     , REPLACE('2021-06-18') AS REPLACE_3
FROM dual;  

/*
    4.1.5. TRIM('제거할 문자' FROM '문자열')
    - 컬럼이나 대상문자열에서 특정 문자가 첫번째 글자이거나 마지막 글자이면 잘라내고 남음 문자열만 반환
*/
SELECT 'Never Mind'
     , TRIM('N' FROM 'Never mind')
  FROM dual;
  
/*
    4.2 숫자함수
     ROUND : 특정 자릿수에서 반올림한다.
     TRUNC : 특정자릿수에서 잘라낸다.(버림)
     MOD : 입력받은 수를 나눈 나머지 값을 반환한다.
*/
 -- 1. ROUND : 특정 자릿수에서 반올림한다.
 -- 두번째 인자값이 0이면 소수점이하 1번째 자리를 반올림하고, -1이면 일의 자리에서 반올림하고, 일의 자리는 0으로 채운다.
SELECT ROUND(98.7654, 2)    --98.77
     , ROUND(98.7654, 1)    --98.8
     , ROUND(98.7654, 0)    --99
     , ROUND(98.7654, -1)   --100
     , ROUND(94.7654, -1)   --90
  FROM dual;

-- 2. TRUNC : 특정자릿수에서 잘라낸다.(버림)
-- 두번째 인자값이 0이면 소수점이하에서 버리고, -1이면 일의 자리에서 버리고, 일의 자리는 0으로 채운다.
SELECT TRUNC(98.7654, 2)    --98.76
     , TRUNC(98.7654, 1)    --98.7
     , TRUNC(98.7654, 0)    --98
     , TRUNC(98.7654, -1)   --90
     , TRUNC(94.7654, -1)   --90
  FROM dual;
  
 -- 3. MOD : 입력받은 수를 나눈 나머지 값을 반환한다.
SELECT MOD(27, 2)  -- 1
     , MOD(27, 5)  -- 2
  FROM dual;
  
 -- 4. ABS : 절대값을 반환하는 함수
SELECT -10     -- -10
    , ABS(-10) -- 10
  FROM dual; 
  
 -- 5. FLOOR 함수 : 소수점 이하를 버리는 함수
SELECT 34.56789      -- 34.56789
     , FLOOR(34.56789)    -- 34
  FROM dual;    
  
/* 03. 날짜함수 - 매우 중요
    p117 표
     구분                     설명
     SYSDATE          시스템에 저장된 현재 날짜를 반환한다.
     MONTHS_BETWEEN   두 날짜 사이가 몇 개월인지를 반환한다.
     ADD_MONTHS       특정 날짜에 개월수를 더한다.
     NEXT_DAY         특정 날짜에서 최초로 도래하는 인자로 받은 요일의 날짜를 반환한다.
     LAST_DAY         해당 달의 마지막 날짜를 반환한다.
     ROUND            인자로 받은 날짜를 특정 기준으로 반올림한다.
     TRUNC            인자로 받은 날짜를 특정 기준으로 버린다.
*/
SELECT sysdate -1 어제
     , sysdate 오늘
     , sysdate +1 내일
     , sysdate +2 모레
  FROM dual;
/*
    1. ROUND : 인자로 받은 날짜를 특정 기준으로 반올림한다.
    - 형식 : ROUND(date, format)
    - format이 'MONTH'인 경우, 월을 기준으로 16보다 작으면 이번달 1일, 16이상이면 다음달 1일
        
*/
SELECT employee_id
     , hire_date 입사일
     , ROUND(hire_date, 'RM') R_입사일
     , sysdate - hire_date 근무일수
     , ROUND(sysdate - hire_date) R_근무일수
  FROM employees; 
  
/*
    1. TRUNC : 인자로 받은 날짜를 특정 기준으로 반올림한다.
    - 형식 : TRUNC(date, format)
    - format이 'MONTH'인 경우, 월을 기준으로 자른다.
    
*/
SELECT employee_id
     , hire_date 입사일
     , TRUNC(hire_date, 'MON') T_입사일
     , sysdate - hire_date 근무일수
     , TRUNC(sysdate - hire_date) T_근무일수
  FROM employees 
 ORDER BY hire_date;

/*  중요
    3. MONTHS_BETWEEN
    - 형식 MONTHS_BETWEEN(date1, date2)
    - 두 날짜 사이가 몇 개월인지를 반환
*/
 -- 사번, 입사일, 근무개월수, R_근무개월수, T_근무개월수
SELECT employee_id
     , hire_date
     , MONTHS_BETWEEN(sysdate, hire_date) 근무개월수
     , ROUND(MONTHS_BETWEEN(sysdate, hire_date)) R_근무개월수
     , TRUNC(MONTHS_BETWEEN(sysdate, hire_date)) T_근무개월수
  FROM employees
 ORDER BY hire_date;

----------------------------------------------
 
 /*
    4. ADD_MONTHS
    - 특정 개월수를 더한 날짜를 구하는 함수
    - 형식 : AD_MONTHS(date, number)
 */
 -- 사번, 입사일, 입사 3개월
 SELECT employee_id 사번
      , hire_date 입사일
      , ADD_MONTHS(hire_date, 3) "입사 3개월"
   FROM employees
  ORDER BY hire_date; 
  
 -- 개강 6개월
 SELECT ADD_MONTHS('21/05/10', 6) "개강 6개월"  
   FROM dual;

/*
    5. NEXT_DAY(date, '요일')
    - NEXT_DAY 함수는 해당 날짜를 기준으로 최초로 도래하는 요일에 해당하는 날짜를 반환하는 함수
    - NEXT_DAY(date, '요일')
    - 요일 대신 숫자가 올 수 있다. 1:일요일 2:월요일 ...
    
*/

SELECT sysdate
     , NEXT_DAY(sysdate, '일요일') SUN
     , NEXT_DAY(sysdate, '금요일') FRI
     , NEXT_DAY(sysdate, '토요일') SAT
     , NEXT_DAY(sysdate, 7) 토
  FROM dual;
  
/*
    6. LAST_DAY
    - LAST_DAY 함수는 해당 날짜가 속한 달의 마지막 날짜를 반환
    - 형식 : LAST_DAY(date)
*/
-- 사원테이블에서 사번, 입사일, "입사한 달의 마지막 날"
SELECT employee_id "사번"
     , hire_date "입사일"
     , LAST_DAY(hire_date) "입사한 달의 마지막 날"
  FROM employees
 ORDER BY hire_date;
 
/*
    p124 매우중요
    4.4 형변환 함수
    - 오라클에서 원하는 데이터형으로 변환해야하는 경우 TO_NUMBER, TO_CHAR, TO_DATE를 사용
    TO_CHAR 날짜형 혹은 숫자형을 문자형으로 변환
    TO_DATE 문자형을 날짜형으로 변환
    TO_NUMBER 문자형을 숫자형으로 변환
*/

/*
    1. TO_CHAR
    - 날짜형 혹은 숫자형을 문자형으로 변환
    - 형식 : TO_CHAR(날짜데이터, '출력형식')
    - 날짜 출력 형식
    종류      의미
    YYYY    년도표현(4자리)
    YY      년도표현(2자리)
    MM      월을 숫자로 표현
    MON     월을 알파벳으로 표현
    DAY     요일 표현
    DY      요일을 약어로 표현
    W       몇 번째 주
    CC      세기
*/
SELECT sysdate  -- 21/06/17
     , TO_CHAR(sysdate, 'MM') AS MM -- 21-06-17 (목요일)
     , TO_CHAR(sysdate, 'MON') AS MON -- 21-06-17 (목요일)
     , TO_CHAR(sysdate, 'MONTH') AS MONTH -- 21/06/17 목
     , TO_CHAR(sysdate, 'DD') AS DD -- 21/06/17 (목)
     , TO_CHAR(sysdate, 'DY') AS DY
     , TO_CHAR(sysdate, 'DAY') AS DAY
     , TO_CHAR(sysdate, 'W') AS 주
     , TO_CHAR(sysdate, 'CC') AS 세기
  FROM dual;  
 -- 21/06/17	06	6월 	6월 	17	목	목요일  

SELECT sysdate  -- 21/06/17
     , TO_CHAR(sysdate, 'YY-MM-DD') "일자" -- 21-06-17 (목요일)
     , TO_CHAR(sysdate, 'YY-MM-DD (DAY)') "일자 (요일)" -- 21-06-17 (목요일)
     , TO_CHAR(sysdate, 'YY/MM/DD DY') "일자 요일" -- 21/06/17 목
     , TO_CHAR(sysdate, 'YY/MM/DD (DY)') "일자 (요일)" -- 21/06/17 (목)
  FROM dual;
 
-- 시간 표현
-- AM 또는 PM
-- 12시간 단위 표시의 경우 -> HH:MM:SS
-- 24시간 단위 표시의 경우 -> HH24:MM:SS
SELECT TO_CHAR(SYSDATE, 'YYYY/MM/DD, HH24:MI:SS') "현재 날짜 시각"     
  FROM dual; 
  
SELECT TO_CHAR(SYSDATE, 'YYYY/MM/DD(DY), HH24:MI:SS') "현재 날짜 요일 시각"     
  FROM dual;
  
SELECT TO_CHAR(SYSDATE, 'YYYY/MM/DD, HH24:MI:SS') AS "HH24:MI:SS"  
     , TO_CHAR(SYSDATE, 'YYYY/MM/DD, HH24:MI:SS AM') AS "HH24:MI:SS AM (오전OR오후)"
     , TO_CHAR(SYSDATE, 'YYYY/MM/DD, HH24:MI:SS PM') AS "HH24:MI:SS PM (오전OR오후)" 
  FROM dual; 
    
/*
    1-1. TO_CHAR
    -- 형식 : TO_CHAR(숫자, '출력형식')
    종류     의미
    L       각 지역별 통화기호를 앞에 표시 예) 한국:￦ (환경설정의 NLS창에서 설정된 통화 기호가 나옴)
    ,       천 단위 자리 구분을 표시
    .       소수점 표시
    9       자리수를 나타내며, 자리수가 맞지 않아도 0으로 채우지 않는다.
    0       자리수를 나타내며, 자리수가 맞지 않을 경우 0으로 채운다.
*/

-- 사번, 급여
SELECT employee_id
     , salary
     , TO_CHAR(salary, '$999,999')
     , TO_CHAR(salary, 'L099,999')
  FROM employees;
  
SELECT sysdate
     , TO_CHAR(sysdate, 'MON', 'NLS_DATE_LANGUAGE = KOREAN') AS MON_KOR
     , TO_CHAR(sysdate, 'MON', 'NLS_DATE_LANGUAGE = JAPANESE') AS MON_JPN
     , TO_CHAR(sysdate, 'MON', 'NLS_DATE_LANGUAGE = ENGLISH') AS MON_ENG
     , TO_CHAR(sysdate, 'MONTH', 'NLS_DATE_LANGUAGE = KOREAN') AS MONTH_KOR
     , TO_CHAR(sysdate, 'MONTH', 'NLS_DATE_LANGUAGE = JAPANESE') AS MONTH_JPN
     , TO_CHAR(sysdate, 'MONTH', 'NLS_DATE_LANGUAGE = ENGLISH') AS MONTH_ENG    
  FROM dual;   
  
SELECT sysdate
     , TO_CHAR(SYSDATE, 'MM') AS MM
     , TO_CHAR(SYSDATE, 'DD') AS MM
     , TO_CHAR(sysdate, 'DY', 'NLS_DATE_LANGUAGE = KOREAN') AS DY_KOR
     , TO_CHAR(sysdate, 'DY', 'NLS_DATE_LANGUAGE = JAPANESE') AS DY_JPN
     , TO_CHAR(sysdate, 'DY', 'NLS_DATE_LANGUAGE = ENGLISH') AS DY_ENG
     , TO_CHAR(sysdate, 'DAY', 'NLS_DATE_LANGUAGE = KOREAN') AS DAY_KOR
     , TO_CHAR(sysdate, 'DAY', 'NLS_DATE_LANGUAGE = JAPANESE') AS DAY_JPN
     , TO_CHAR(sysdate, 'DAY', 'NLS_DATE_LANGUAGE = ENGLISH') AS DAY_ENG     
  FROM dual;   

/*
    p128 4.2 TO_DATE 함수
    -- 문자열을 날짜형으로 변환
    -- 형식 : TO_DATE('문자', '날짜 format')
*/
SELECT employee_id
     , TO_CHAR(hire_date, 'YY/MM/DD')
  FROM employees
 WHERE hire_date = TO_DATE(20040614, 'YYYYMMDD'); 

/*
    p129 4.3 TO_NUMBER 함수
    -- 형식 : TO_NUMBER('문자', '숫자 포맷')
    -- 문자형을 숫자형으로 변환
*/
SELECT '100,000', '999,999' - '50,000'
  FROM dual;  -- 오류 문자열은 산수불가

SELECT to_number('100,000','999,999') - to_number('50,000','999,999') "100,000-50,000"
  FROM dual; 

/*
p130
    5 일반 함수
    5-1. nvl 함수
    5-2. nvl2 함수
    5-3. nullif 함수
    5-4 coalesce 함수
*/

/*
    5-1 nvl함수 -> 매우 중요
    -null은 정해지지 않은 혹은 무한대의 의미이기 때문에 연산,할당,비교가 불가능하다.
    연산시 관계 컬럼도 null로 바뀐다.
    nvl : null인 경우 연산,할당, 비교가 불가능하므로, null인 경우 nvl을 이용해  다른 값으로 대체
    문법 : nvl(컬럼명 , 초기값)
        단 두개의 값은 반드시 데이터 타입이 일치해야 한다.
*/

--사원 테이블에서 commission_pct 가 null이 아닌 경우의 사번, last_name, salary, commission_pct,"커미션 null 연봉", "nvl 연봉"을 구하시오
--연봉 : 급여 * 12 + commission_pct 
-- 단 commission_pct 가 null인 경우 0으로 계산
-- nvl 적용시 , commission_pct가 nll인 경우 0으로 대체되어 연봉계산시 정상적으로 계산된다.

select employee_id
        ,last_name
        ,salary
        ,commission_pct
        ,salary* (12 + commission_pct) as "커미션 null 연봉"
        ,salary* (12 + nvl(commission_pct,0 )) as "nvl 연봉"
FROM employees;

-- locations에서 location_id, city,state_province 출력, (단 state_province null인 경우 "서울"로 출력)
SELECT location_id 
        ,city
        ,state_province
        ,nvl(state_province,'서울')
from locations;

-- locations에서 location_id, city,state_province 출력, (단 state_province null이 아닌 경우)
SELECT location_id 
        ,city
        ,state_province
from locations
where state_province is not null;

/*
    5-2 nvl2 함수
    -형식 : nvl2(expr1, expr2 ,expr3)
    -expr1을 검사하여 그 결과가 null이면 expr3을 반환하고, null이 아니면 expre2를 반환 
*/
--사원 테이블에서 사번, last_name, salary, commission_pct,"커미션 null 연봉", "nvl2 연봉"을 구하시오
--연봉 : 급여 * 12 + commission_pct 
-- 단 commission_pct 가 null인 경우 0으로 계산
-- nvl 적용시 , commission_pct가 nll인 경우 0으로 대체되어 연봉계산시 정상적으로 계산된다.
-- 단 연봉 : commission_pct가 null이면 salary*12
          --commission_pct가 null이 아니면 salary * (12 + commission_pct) 

select employee_id
        ,last_name
        ,salary
        ,commission_pct
        ,salary * (12 + commission_pct)
        ,nvl2(commission_pct , salary*(12+commission_pct) , salary*12) as "nvl2 연봉"
FROM employees;

/*
    5.3 NULLIFF 함수
    형식 : NULLIF(expr1, expr2)
    두 표현식을 비교하여 동일한 경우에는 null을 반환하고, 동일하지 않으면 첫 번째 표현식을 반환
*/ 
select nullif('A', 'A')
     , nullif('A', 'X')
  from dual;   
  
/*
    5.4. COALESCE 함수
    -- 형식 : COALESCE(expr1, expr2.. expr-n)
    -- 인수 중에서 NULL이 아닌 첫 번째 인수를 반환하는 함수
*/
select employee_id
     , last_name
     , salary
     , commission_pct
     , salary* (12 + commission_pct) AS "커미션 null 연봉"
     , COALESCE(commission_pct, salary, 0)
FROM employees;

/*
    [3일차] p134
    5.5 DECODE 함수
    - 자바의 switch case문과 동일
    - 조건에 따라 다양한 선택이 가능
    - 형식 : DECODE(표현식, 조건1, 결과1
                        , 조건2, 결과2
                        , 조건3, 결과3
                        , 기본결과n);
*/

-- 사원테이블의 부서ID가 10~50인 경우 부서 ID와 '부서명' 출력 그 외에는 '부서미정'으로 출력
-- 부서 ID로 정렬하며 부서 ID는 중복되지 않아야하며, 부서 ID가 null이면 출력되지 않아야 한다.
-- 10 Marketing
-- 20 Purchasing
-- 30 Human Resource
-- 40 Shipping
-- 50 IT
SELECT DISTINCT department_id
     , DECODE(department_id, 10, 'Marketing'
                           , 20, 'Purchasing'
                           , 30, 'Human Resource'
                           , 40, 'Shipping'
                           , 50, 'IT'
                           , '부서미정') AS "부서명"
  FROM employees
 WHERE department_id IS NOT NULL 
 ORDER BY department_id;

/*
    p135 CASE 함수
    - 자바의 if else와 유사한 구조를 가진다.
    - 형식 :
    CASE 표현식
        WHEN 조건1 THEN 결과1
        WHEN 조건2 THEN 결과2
        WHEN 조건3 THEN 결과3
        ...
        ELSE 결과N
    END; 
    
*/

-- 사원테이블의 부서ID가 10~50인 경우 부서 ID와 '부서명' 출력 그 외에는 '부서미정'으로 출력
-- 부서 ID로 정렬하며 부서 ID는 중복되지 않아야하며, 부서 ID가 null이면 출력되지 않아야 한다.
-- 10 Marketing
-- 20 Purchasing
-- 30 Human Resource
-- 40 Shipping
-- 50 IT
SELECT DISTINCT department_id
     , CASE department_id WHEN 10 THEN 'Marketing'
                          WHEN 20 THEN 'Purchasing'
                          WHEN 30 THEN 'Human Resource'         
                          WHEN 40 THEN 'Shipping'
                          WHEN 50 THEN 'IT'
                          ELSE '부서미정' 
        END AS "부서명"
  FROM employees
 WHERE department_id IS NOT NULL 
 ORDER BY department_id;