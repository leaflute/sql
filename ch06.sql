/*  ch06
	[면접]
	EQUI JOIN 매우중요
	- 조인 대상 테이블에서 공통 컬럼을 '=(equal) 비교를 통해 같은 값을 가지는 행렬을 연결
    - 정의: 가장 많이 사용되는 조인 방법으로, 조인 대상이 되는 두 테이블에서 공통적으로
    존재하는 컬럼의 값이 일치하는 행을 연결하여 검색결과를 생성하는 방법
    - FROM절은 조인 대상 테이블을 기술하고, 테이블은 콤마(,)로 구분
    - WHERE절은 조인 위한 컬럼명과 '='연산자를 이용해 조인조건을 기술
    
    - 기본키(PK: primary key) : 중북되지 않아야 하며, null(x)
    - 외래키(FK: foreign key) : 다른 테이블의 키를 참조, null 일수도
    예) 기본키(부서테이블.부서ID) 외래키(사원테이블,사원키)
         부모테이블(부서테이블)    자식테이블(사원테이블)
         
    - 중복컬럼(모호한 컬럼)일 경우 컬럼명 앞에 테이블명(또는 별칭)을 기술해
    어떤 테이블 소속인지 구분
    
    
	- 형식: 
	SELECT t1.column, t2. colum
	FROM table1 t1, table1 t2
	WHERE t1.column = t2.column
	AND 조건식;
*/

DESC employees;

SELECT *
  FROM departments;

SELECT DISTINCT department_id
  FROM employees 
 ORDER BY department_id;
 
-- 부서정보  
-- 카디시안곱
SELECT e.employee_id
     , e.department_id
     , d.department_name
  FROM departments d, employees e
 WHERE e.employee_id = 100; 


-- 사원테이블, 부서테이블에서 부서번호가 100번인 사원의 사번, 부서번호, 부서명
SELECT e.employee_id
     , d.department_id
     , d.department_name
  FROM employees e, departments d
 WHERE e.department_id = d.department_id
   AND e.department_id = 100;
/*
    JOIN-ON
    테이블명과 테이블명 사이에 콤마(,) 대신 JOIN을 사용하고
    공통으로 존재하는 키를 비교하기 위해 WHERE대신 ON을 사용
    다른 검색과 필터조건은 WHERE절에 분리해서 기술
    
    - 형식
    SELECT t1.column, t2,column
    FROM table1 t1 JOIN table1 t2
    
*/
-- 위의 예제를 join on으로 적용
SELECT e.employee_id
     , d.department_id
     , d.department_name
  FROM employees e JOIN departments d
    ON e.department_id = d.department_id
   WHERE e.department_id = 100;

-- 직무정보
-- 사원테이블에서 100번인 사원의 사번과, jobs 테이블의 모든 정보 조회
SELECT e.employee_id
     , j.*
  FROM employees e, jobs j
 WHERE e.employee_id = 100;
-- JOIN ON
SELECT e.employee_id
     , j.*
  FROM employees e JOIN jobs j
    ON e.employee_id = 100;

-- 위치 정보
-- 부서ID가 10번일 때의 부서ID, 부서명, location정보
SELECT d.department_id
     , d.department_name
     , l.*
  FROM departments d, locations l
 WHERE d.location_id = l.location_id;
-- JOIN ON 
SELECT d.department_id
     , d.department_name
     , l.*
  FROM departments d JOIN locations l
    ON d.location_id = l.location_id;  

-- 국가 정보
-- location_id가 1000일때의 street_adress, city, contries 정보
SELECT l.street_address
     , l.city
     , c.*
  FROM locations l, countries c
 WHERE l.country_id = c.country_id; 
 
SELECT l.street_address
     , l.city
     , c.*
  FROM locations l JOIN countries c
    ON l.country_id = c.country_id;  
    
-- 지역 정보
-- country_id에 US가 포함된 지역id와 이름을 조회
SELECT c.country_id
     , r.*
  FROM countries c, regions r
 WHERE r.region_id = c.region_id
   AND c.country_id LIKE '%US%';

SELECT c.country_id
     , r.*
  FROM countries c JOIN regions r
    ON r.region_id = c.region_id
 WHERE c.country_id LIKE '%US%';   

-- IT가 포함된 부서명을 가진 부서의 사번, 이름(last_name + first_name), 입사일, 부서코드, 부서명 조회
SELECT e.employee_id
     , CONCAT(e.first_name, e.last_name)
     , e.hire_date
     , e.department_id
     , d.department_name
  FROM employees e, departments d
 WHERE e.department_id = d.department_id
   AND d.department_name LIKE '%IT%';
 
SELECT e.employee_id
     , CONCAT(e.first_name, e.last_name)
     , e.hire_date
     , e.department_id
     , d.department_name
  FROM employees e JOIN departments d
    ON e.department_id = d.department_id
 WHERE d.department_name LIKE '%IT%';   

-- 'seattle'이라는 'city'에서 근무하는 사원의 사번, last_name, salary
-- 부서ID, 부서명, city 조회
SELECT e.employee_id
     , e.last_name
     , e.salary
  FROM employees e
     , departments d
     , locations l
 WHERE e.department_id = d.department_id
   AND d.location_id = l.location_id
   AND l.city = 'Seattle';

SELECT e.employee_id
     , e.last_name
     , e.salary
  FROM employees e
  JOIN departments d
    ON e.department_id = d.department_id
  JOIN locations l
    ON d.location_id = l.location_id
 WHERE l.city = 'Seattle';  
 
 -- 사번이 101번, job_history의 start_date가 '97/09/21'인 사원의 사번, last_name
-- 부서정보, 위치정보, 국가정보, 지역정보, 직무정보, 직무history 가져오기
SELECT e.employee_id
     , e.last_name
     , d.department_name
     , l.city
     , c.country_name
     , r.region_name
     , j.job_title
     , jh.*
  FROM employees e
     , departments d
     , locations l
     , countries c
     , regions r
     , jobs j
     , job_history jh
 WHERE e.department_id = d.department_id
   AND d.location_id = l.location_id
   AND l.country_id = c.country_id
   AND c.region_id = r.region_id
   AND j.job_id = e.job_id
   AND e.employee_id = jh.employee_id
   AND e.employee_id = 101
   AND jh.start_date = '97/09/21';
   
SELECT e.employee_id
     , e.last_name
     , d.department_name
     , l.city
     , c.country_name
     , r.region_name
     , j.job_title
     , jh.*
  FROM employees e
  JOIN departments d
    ON e.department_id = d.department_id
  JOIN locations l
    ON d.location_id = l.location_id
  JOIN countries c
    ON l.country_id = c.country_id
  JOIN regions r
    ON c.region_id = r.region_id
  JOIN jobs j
    ON j.job_id = e.job_id
  JOIN job_history jh
    ON e.employee_id = jh.employee_id
 WHERE e.employee_id = 101
   AND jh.start_date = '97/09/21';   
   
----------   
/*
    p173 4. SELF JOIN
    - 하나의 테이블에 있는 컬럼끼리 연결해야하는 조인이 필요한 경우 사용
    - 자기 자신과 조인을 맺는 것
    - 같은 테이블이 하나 더 존재하는 것 처럼 여겨, 하나의 테이블에 다른 별칭을 사용
    - 사원 테이블에서 manager_id 컬럼이 존재하는데 이 컬럼은 각 사원의 담당매니저의 employee_id
    - 사원 테이블: employees e, 매니저 테이블: employees m
    - 같은 테이블이지만 매니저도 사원이므로 e.manager_id = m.employee_id 비교 가능
*/
-- 156사원의 사번, last_name, 매니저_Id, 매니저_last_name
SELECT e.employee_id "사번"
     , e.last_name "사원"
     , e.manager_id "담당 매니저 id"
     , m.last_name "담당 매니저 이름"
  FROM employees e, employees m
 WHERE e.manager_id = m.employee_id
   AND e.employee_id = 156;
 
SELECT e.employee_id "사번"
     , e.last_name "사원"
     , e.manager_id "담당 매니저 id"
     , m.last_name "담당 매니저 이름"
  FROM employees e join employees m
    ON e.manager_id = m.employee_id
 WHERE e.employee_id = 156;  
 
/*
    p175 5.OUTER JOIN [면접]
    EQUI JOIN에서 양측 컬럼값 중의 하나가 NULL이지만, 조인 결과로 출력할 필요가 있는 경우에 OUTER JOIN을 사용
    - 두개 이상의 테이블이 조인될 때 어느한쪽의 테이블(부서)에는 해당 데이터가 존재하는데,
    다른 테이블(사원)에는 데이터가 존재하지 않는 경우, 부서 데이터가 출력되지 않는 문제를 해결하기 위한 조인이다.
    NULL 값을 가진 행은 조인결과로 얻어지지 않는다.
    NULL에 대해서 어떤 연산을 적용하더라도 연산('=')결과는 NULL이기 때문이다.
    따라서 OUTER JOIN을 이용하면 NULL이기에 배제된 행을 결과에 포함할 수 있으며,
    (+)기호를 조인조건에서 정보가 부족한 테이블의 컬럼이름뒤에 붙인다.
    
    RIGHT, LEFT, FULL 3가지가 있다.
    - RIGHT OUTER JOIN : 테이블 기준은 오른쪽 기준이며, 왼쪽 테이블에 조인시킬 컬럼의 값이 없는 경우에 사용
                        기준이 되는 쪽은, 반드시 데이터가 모두 출력되어야 한다.
    => 왼쪽 테이블 컬럼뒤에(+)를 추가한 것과 동일(FROM 절의 왼쪽 테이블이 NULL 이더라도 출력되어야 한다.)
      
    - LEFT OUTER JOIN : 테이블 기준은 왼쪽 기준이며, 오른쪽 테이블에 조인시킬 컬럼의 값이 없는 경우에 사용
                        기준이 되는 쪽은, 반드시 데이터가 모두 출력되어야 한다.
    => 오른쪽 테이블 컬럼뒤에(+)를 추가한 것과 동일(FROM 절의 오른쪽 테이블이 NULL 이더라도 출력되어야 한다.)    
      
    - FULL OUTER JOIN : 양쪽 테이블 모두 OUTER JOIN 걸어야 하는 경우 사용한다.
    => 양쪽 테이블 컬럼뒤에 (+)를 추가     
*/
-- 부서ID : 사원테이블(10~110)
SELECT DISTINCT department_id FROM employees
 ORDER BY department_id; 
 
-- 부서ID : 부서테이블(10~270)
SELECT DISTINCT department_id FROM departments;

-- 120~170번 부서가 신설부서, 부서정보 출력
-- 사원 테이블의 부서코드가 null이어도 부서테이블의 부서정보를 출력
-- NULL이 있는 곳에 (+)를 줘야함
SELECT DISTINCT e.department_id
     , d.*
  FROM employees e, departments d
 WHERE e.department_id(+) = d.department_id
 ORDER BY e.department_id;

SELECT DISTINCT e.department_id
     , d.*
  FROM employees e JOIN departments d
    ON e.department_id(+) = d.department_id
 ORDER BY e.department_id;

-- LEFT/RIGHT의 구분  
SELECT DISTINCT e.department_id
     , d.*
  FROM departments d LEFT OUTER JOIN employees e
    ON d.department_id = e.department_id 
 ORDER BY e.department_id;   