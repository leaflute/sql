--
/*
    ch10. 데이터 무결성과 제약조건 - 중요, 면접
    - 제약조건이란 테이블에 유효하지 않은(부적절한) 데이터가 입력되는 것을 방지하기 위해서
    테이블 생성시 각 컬럼에 대해 정의하는 규칙
    - Oracledml 제약조건 종류
        구분                  설명
    NOT NULL - 컬럼에 NULL값을 포함하지 못하도록 지정한다.
    UNIQUE  - 테이블의 모든 로우에 대해서 유일한 값을 갖도록 한다.
    PRIMARY KEY - 테이블의 각 행을 식별하기 위한 것으로 NULL과 중복된 값을 모두 허용하지 않는다.
                즉, NOT NULL 조건 + UNIQUE
    FOREIGN KEY - 참조되는 테이블에 컬럼값이 항상 존재해야 한다.
    CHECK - 저장 가능한 데이터 값의 범위나 조건을 지정하여 설정한 값만을 허용한다.
    
    제약조건명 : 중복을 허용하지 않는다.(유일해야 한다.) 테이블명_컬럼명_키
    - 제약조건에 따른 INSERT, DELETE.. 중요(INSERT는 부모테이블부터, DELETE는 자식테이블부터)
    1. 부모테이블의 PK컬럼(부서.부서번호)에 데이터가 존재해야
       자식테이블의 FK 컬럼(사원.부서번호)에 데이터가 INSERT 가능하다.
       
    2. 자식테이블의 FK 컬럼(사원.부서번호)로 사용되는 행이 존재하면, 자식 FK데이터를 먼저 삭제한 후
       부모데이터를 삭제한다.
       
    3. ON DELETE CASCADE :  자식테이블에 설정하면, 부모테이블데이터 삭제시 자식도 함께 삭제    
*/

-- 테이블 레벨
SELECT * FROM tab;

-- 부서 테이블 생성
DROP TABLE dept;
CREATE TABLE dept(
    deptno  NUMBER(3),
    deptname    VARCHAR2(50) NOT NULL,
    loc         VARCHAR2(50),
    CONSTRAINT dept_depno_pk PRIMARY KEY(deptno)    -- 테이블 레벨 제약조건
);

-- 사원 테이블 생성
DROP TABLE emp;
CREATE TABLE emp(
    empno   NUMBER(4),
    ename   VARCHAR2(20) NOT NULL,
    hire_date DATE DEFAULT sysdate,
    salary  NUMBER(9) CONSTRAINT emp_salary_min CHECK(salary > 0),
    deptno  NUMBER(3),
    email   VARCHAR2(60),
    CONSTRAINT emp_empno_pk PRIMARY KEY(empno),
    CONSTRAINT emp_deptno_fk FOREIGN KEY(deptno) REFERENCES dept(deptno)
        ON DELETE CASCADE,  -- 자식테이블에 설정 부모테이블 삭제하면 자식테이블에서도 삭제
    CONSTRAINT emp_email_uq UNIQUE(email)     
);

SELECT * FROM tab;

-- INSERT : dept -> emp
INSERT INTO dept(deptno, deptname, loc)
VALUES(101,'IT','뉴욕');

INSERT INTO dept(deptno, deptname, loc)
VALUES(102,'POLICE','서울');

INSERT INTO dept(deptno, deptname, loc)
VALUES(103,'ACCOUNT','파리');

INSERT INTO dept(deptno, deptname, loc)
VALUES(104,'HR','프랑크푸르트');

INSERT INTO dept(deptno, deptname, loc)
VALUES(105,'MARKETING','파리');

INSERT INTO dept(deptno, deptname, loc)
VALUES(106,'PRODUCE','런던');

COMMIT;
SELECT * FROM dept;

INSERT INTO emp(empno, ename, hire_date, salary, deptno, email)
VALUES(1001, '홍길동', '2021/06/22', 10000, 101, 'hong@naver.com');

INSERT INTO emp(empno, ename, hire_date, salary, deptno, email)
VALUES(1002, '전우치', '2021/05/22', 20000, 102, 'jeon@naver.com');

INSERT INTO emp(empno, ename, hire_date, salary, deptno, email)
VALUES(1003, '임꺽정', '2020/06/22', 5000, 103, 'lim@naver.com');

INSERT INTO emp(empno, ename, hire_date, salary, deptno, email)
VALUES(1004, '척준경', '2019/07/22', 10000, 104, 'chuk@naver.com');

INSERT INTO emp(empno, ename, hire_date, salary, deptno, email)
VALUES(1005, '허생', '2020/06/23', 9000, 105, 'heo@naver.com');

---
INSERT INTO emp(empno, ename, hire_date, salary, deptno, email)
VALUES(1006, '을파소', '2020/06/23', 9000, 109, 'eul@naver.com'); -- fk error

-- PK인 106항목을 추가후 

INSERT INTO emp(empno, ename, hire_date, salary, deptno, email)
VALUES(1006, '고담덕', '2018/12/24', 12000, 106, 'go@naver.com');

INSERT INTO emp(empno, ename, hire_date, salary, deptno, email)
VALUES(1007, '양만춘', '2018/12/24', -1, 105, 'yang@naver.com'); -- 양수 에러

SELECT * FROM emp;
COMMIT;

--- 컬럼 레벨
-- 부서 테이블 생성
DROP TABLE dept_col;
CREATE TABLE dept_col(
    deptno      NUMBER(3)       PRIMARY KEY,
    deptname    VARCHAR2(50)    NOT NULL,
    loc         VARCHAR2(50)
--    CONSTRAINT dept_depno_pk PRIMARY KEY(deptno)    -- 테이블 레벨 제약조건
);

-- 사원 테이블 생성
DROP TABLE emp_col;
CREATE TABLE emp_col(
    empno   NUMBER(4)       PRIMARY KEY,
    ename   VARCHAR2(20)    NOT NULL,
    hire_date DATE          DEFAULT sysdate,
    salary  NUMBER(9)       CONSTRAINT emp_col_salary_min CHECK(salary > 0),
    deptno  NUMBER(3)       REFERENCES dept_col(deptno) ON DELETE CASCADE,
    email   VARCHAR2(60)    UNIQUE
--    CONSTRAINT emp_empno_pk PRIMARY KEY(empno),
--    CONSTRAINT emp_deptno_fk FOREIGN KEY(deptno) REFERENCES dept(deptno)
--        ON DELETE CASCADE, -- 자식테이블에 설정하고 부모테이블에서 해당 로우를 삭제하면 자식테이블에서도 삭제
--    CONSTRAINT emp_email_uq UNIQUE(email)     
);

SELECT * FROM tab;

DELETE dept WHERE deptno = 106; -- ON DELETE CASCADE때문에 자식테이블에서 deptno가 106인 데이터가 전부 삭제됨

-- 제약조건 정보 가져오기
-- user_로 시작하는 데이터사전 테이블
SELECT constraint_name, constraint_type, table_name
  FROM sys.user_constraints
 WHERE table_name IN ('EMP', 'DEPT', 'EMP_COL', 'DEPT_COLL'); -- 테이블 명은 반드시 대문자 