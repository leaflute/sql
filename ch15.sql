/*
    p364 CH15 프로시저 (매우 중요)
    - 프로시저 : PL/SQL은 프로그램 로직을 프로시저로 구현하여 객체 형태로 사용한다.
                 프로시저는 일반 프로그래밍 언어에서 사용하는 함수와 비슷한 개념으로
                 작업순서가 정해진 독립된 프로그램의 수행단위를 말한다.
                 프로시저는 정의된 다음, 오라클(DBMS)에 저장되므로 저장프로시저(STORED PROCEDURE)라고도 한다.
    - 오라클에서 함수는 반드시 RETURN문을 사용하여 결과를 반환하지만, 프로시저는 결과를 반환할수도, 반환하지 않을 수도 있다.
    - CallableStatement : DB에 생성된 저장프로시저를 execute()로 호출해서 사용가능.. jsp, spring에서 호출
        
    [형식]
    CREATE [OR REPLACE] PROCEDURE 프로시저명(매개변수 data_type, ...)
    IS
        로컬변수;   -- 변수선언
    BEGIN
        statement1;
        statement2;
    END;
    - 실행 : EXECUTE 프로시저명;
    - 삭제 : DROP PROCEDURE 프로시저명;
    
    - [MODE]는 IN,OUT,INOUT 세가지를 기술할 수 있다.
    IN : 프로시저로 데이터를 전달받을때 사용
    OUT : 프로시저에서 수행된 결과값을 전달할 때 사용
    INOUT : 두가지 목적에 모두 사용
*/
-- last_name='Nayer'의 급여 출력
-- '/'까지 블록을 잡아서 컴파일한 후 EXECUTE문 실행
-- HR 계정에서 실행
SET SERVEROUTPUT ON;
CREATE OR REPLACE PROCEDURE sp_salary
IS
v_salary employees.last_name%type;
BEGIN
    SELECT salary INTO v_salary
      FROM employees
     WHERE last_name = 'Nayer';
    DBMS_OUTPUT.PUT_LINE('Nayer 의 급여는 '||v_salary);
END;
/
EXECUTE sp_salary;

SELECT name, text FROM user_source
 WHERE NAME LIKE('SP_SALARY');
 
-- p367
-- IN 매개변수 사용하기
SET SERVEROUTPUT ON;
CREATE OR REPLACE PROCEDURE sp_salary_ename(
v_ename IN employees.last_name%type
)
IS
v_salary  employees.salary%type; -- 로컬변수
BEGIN
    SELECT salary INTO v_salary
      FROM employees
     WHERE last_name = v_ename; 
     dbms_output.put_line(v_ename||'의 급여는 '||v_salary);
END;
/

EXECUTE sp_salary_ename('Nayer');
EXECUTE sp_salary_ename('Chen');

-- p368 
-- /까지 블럭

SET SERVEROUTPUT ON;
CREATE OR REPLACE PROCEDURE sp_salary_ename2(
    v_ename IN employees.last_name%type,
    v_salary OUT employees.salary%type
)
IS
BEGIN
    SELECT salary INTO v_salary
      FROM employees
     WHERE last_name = v_ename; 
     dbms_output.put_line(v_ename||'의 급여는 '||v_salary);
END;
/

VARIABLE v_salary NUMBER;
EXECUTE sp_salary_ename2('Nayer', :v_salary);
PRINT v_salary;

--
/*
    p370 2. 함수
    - 함수는 결과값을 되돌려주기 위한 용도로 사용
    - 형식
    CREASTE [OR REPLACE] FUNCTION 함수이름(
        매개변수1 [mode] 데이터타입,
        매개변수2 [mode] 데이터타입,...
    )
        RETURN 데이터타입
    IS
    BEGIN
        statement1;
        statement2;
    END;
    /
    EXECUTE : variable_name := funtion_name(매개변수_list);
    -- 함수는 결과를 되돌려받기 위해 되돌려받을 자료형과 값을 기술    
*/
-- HR계정에서 실행
-- 'Chen'의 급여를 함수를 이용해 출력
-- /까지 블록잡고 컴파일 -> 실행
CREATE OR REPLACE FUNCTION fn_salary_lastname(
    v_ln IN employees.last_name%type)
    RETURN number
IS
    v_sal NUMBER(7,2);
BEGIN
    SELECT salary INTO v_sal
      FROM employees
     WHERE last_name = v_ln;
    RETURN v_sal; 
END;
/
VARIABLE v_sal NUMBER;
EXECUTE :v_sal := fn_salary_lastname('Chen');
PRINT v_sal;

SELECT last_name
     , salary
     , fn_salary_lastname('Chen')
  FROM employees
 WHERE last_name = 'Chen'; 

/* 
    p373 3.트리거 
    - 트리거 사전적인 의미 : 총의 방아쇠, 연쇄반응, 일련의 사건등을 유발하는 계기, 유인, 자극
    - 오라클의 트리거는 어떤 이벤트가 발생했을 때 자동 실행 되도록 하기 위해 데이터베이스에 저장된 프로시저
    - 형식
    CREATE  TRIGGER 트리거명
    타이밍 [BEFORE|AFTER]  이벤트[INSERT|UPDATE|DELETE]
    ON 테이블명
    [FOR EACH ROW]
    [WHEN conditions]
    BEGIN
        statement;  -- 트리거 문장
    END;
    /
    
    - <트리거의 타이밍>
    - [BEFORE] 타이밍은 어떤 테이블에 INSERT, UPDATE, DELETE 문이 실행될 때(EVENT)
      해당 문장이 실행되기 전에 트리거가 가지고 있는 BEGIN~END 사이의 문장(트리거문장)을 실행
    - [AFTER] 타이밍은 어떤 테이블에 INSERT, UPDATE, DELETE 문이 실행될 때(EVENT)
      해당 문장이 실행되고 난 후에 트리거가 가지고 있는 BEGIN~END 사이의 문장(트리거문장)을 실행
      
    - <트리거의 이벤트>
    - 사용자가 어떤 DML(INSERT, UPDATE, DELETE) 문을 실행했을 때
      트리거를 발생시킬 것인지를 결정한다.
    - 트리거의 몸체
      해당 타이밍에 해당 이벤트가 발생하게 되면 실행될 기본 로직이 포함되는 부분으로 
      BEGIN~END에 기술한다.
*/
-- SCOTT 계정에서 생성 
PURGE RECYCLEBIN;
SELECT * FROM TAB;

CREATE TABLE dept_original
AS
SELECT * FROM dept
 WHERE 0=1;

SELECT * FROM dept_original;

CREATE TABLE dept_copy
AS
SELECT * FROM dept
 WHERE 0=1;

-- 삽입 트리거 
CREATE OR REPLACE TRIGGER trg_dept_copy_insert  -- 자동 COMMIT
    AFTER INSERT
       ON dept_original
    FOR EACH ROW
DECLARE
--    PRAGMA AUTONOMOUS_TRASACTION; -- 주 트랜잭션이나 다른 트랜잭션에 영향을 받지 않고, 독립적으로 COMMIT, ROLLBACK역할
BEGIN
    INSERT INTO dept_copy
    VALUES(:new.deptno, :new.deptname, :new.loc);
END;
/
-- :new는 새로게 추가된 값을 가지고 있는 임시테이블로 원본 테이블에 
-- INSERT가 수행된 후 새로운 그 데이터가 저장될 테이블

INSERT INTO dept_original
VALUES(106, 'DEVELOPMENT', '런던'); 
INSERT INTO dept_original
VALUES(107, 'PRODUCE', '도쿄'); 

SELECT * FROM dept_original;
SELECT * FROM dept_copy;

-- 삭제 트리거
CREATE OR REPLACE TRIGGER trg_dept_copy_delete
    AFTER DELETE
       ON dept_original
    FOR EACH ROW
BEGIN
    DELETE FROM dept_copy
     WHERE dept_copy.deptno = :old.deptno;
END;
/

DELETE FROM dept_original
 WHERE deptno = 107;