/*
    - NULL : 미확정, 값이 정해져 있지 않아 알 수 없는 값을 의미하며, 연산, 비교, 할당이 불가능하다
    연산시 관계 컬럼값도 null로 바뀐다.
    - 컬럼에 별칭 지정
    1) 컬럼명과 별칭 사이에 공백을 추가
    2) 컬럼명과 별칭 사잉 AS를 추가
    3) ""를 사용
    
    - nvl(calumn, value) => column의 값을 value로 치환
*/

-- 중복된 데이터를 한 번씩만 출력하는 DISTINCT 
-- 사원테이블의 부서번호를 검색, (부서번호 오름차순 정렬, 단 중복시 한 번만 출력)
SELECT DISTINCT department_id
  FROM employees
 ORDER BY department_id; 

-- DUAL 테이블 : 한 행으로 결과를 출력하기 위해 오라클에서 제공하는 테이블
-- DUMMY VARCHAR2(1) 라는 하나의 collumn으로 구성되어 있고, 데이터는 'X'값
DESC dual;

-- p45
/*
    비교연산자
        - 같다 : =
        - 같지 않다 : <>, !=, ^=
        - > , < , <= , >=
*/
-- 문자/날짜 데이터 조회
-- 사원테이블에서 last_name이 King이거나 입사일이 05/07/16인 사원의 사번, last_name, 입사일 검색
SELECT employee_id
     , last_name
     , hire_date
  FROM employees
 WHERE last_name = 'KING'
    OR hire_date = '05/07/16';

-- 사원테이블에서 salary가 3000보다 크고 5000보다 작은 사원의 사번, salary 검색
SELECT employee_id 
     , salary
  FROM employees
 WHERE salary > 3000
   AND salary < 5000;

/* 
    p51
    BETWEEN AND 연산자(매우중요)
    - 형식  
    컬럼명 BETWEEN A AND B : 컬럼의 데이터값이 하한값(A)과 상한값(B) 사이에 포함되는 로우를 검색하기 위한 연산자
    컬럼명 NOT BETWEEN A AND B : 컬럼의 데이터값이 하한값(A)과 상한값(B) 사이에 포함되지 않는 로우를 검색하기 위한 연산자
*/ 

-- 사원테이블에서 사번, 입사일 조회(단, 입사일이 03/01/01~04/12/31 제한), 입사일 오름차순 정렬
SELECT employee_id
     , hire_date    
  FROM employees
 WHERE hire_date BETWEEN '03/01/01' AND '04/12/31'
 ORDER BY hire_date;
 
/*
    p53
    IN 연산자 : 특정 컬럼의 값이 A, B, C 중 하나라도 일치하면 참이 되는 연산자이다.
    형식 :  컬럼값 IN(A,B,C);
    형식 :  컬럼값 NOT IN(A,B,C);
*/

-- 사원테이블에서 부서ID가 70,80,90일 때의 사번, 이름, 부서ID로 조회, 부서ID로 정렬
SELECT employee_id
     , last_name
     , department_id
  FROM employees
 WHERE department_id IN(70, 80, 90);

/*
    p55
    LIKE 연산자와 와일드카드
    - 컬럼명 LIKE Pattern
    - 와일드 카드 : % => 하나 이상의 문자가 어떤 값이 와도 상관없다
                  _ => 하나의 문자가 어떤 값이 와도 상관없다
*/

-- 사원 테이블에서 last_name의 3번째, 4번째 단어가 'tt인 사원의 last_name조회
SELECT last_name
  FROM employees
 WHERE last_name LIKE '__tt%'; 

-- 사원 테이블에서 'JONES'가 포함된 email 조회
SELECT email
  FROM employees
 WHERE email LIKE '%JONES%'; 

-- JOBS 테이블에서 'REP'가 포함된 job_id 조회
SELECT job_id
  FROM jobs
 WHERE job_id LIKE '%REP%'; 

-- 사원 테이블에서 'AM'이 포함되어 있지 않은 last_name 조회
SELECT last_name
  FROM employees
 WHERE last_name NOT LIKE '%AM%';

 /*
    p58
    1.7. NULL 연산자를 이용한 조건 검색
    대상컬럼 IS NULL
    대상컬럼 IS NOT NULL
    
    NULL : 미확정, 알 수 없는 값을 의미하며, 연산, 할당, 비교가 불가능하므로 'IS NULL'을 사용한다.
           연산시 관계 컬럼도 NULL로 바뀐다.
    IS NULL 연산자 : 컬럼값 중에서 NULL을 포함하는 로우를 검색하기 위해 사용하는 연산자
*/

-- 사원 테이블에서 commision_pct가 null이 아닌 경우, 사번, 이름, salary, commission_pct 검색
SELECT employee_id
     , last_name
     , salary
     , commission_pct
  FROM employees
 WHERE commission_pct IS NOT NULL; 

-- 사원 테이블에서 department_id가 null인 경우, 사번, 이름, department_id 검색
SELECT employee_id
     , last_name
     , department_id
  FROM employees
 WHERE department_id IS NULL; 

-- 사원 테이블에서  department_id가 null이 아닌 경우, 사번, 이름, department_id 검색
SELECT employee_id
     , last_name
     , department_id
  FROM employees
 WHERE department_id IS NOT NULL; 
 
-- 사원테이블에서 manager_id가 null이 아닌 경우 사번, last_name, salary, hire_date, manager_id 검색
-- (단 입사일순으로 정렬하되, 동일시 급여로 오름차순정렬) 
SELECT manager_id
     , last_name
     , salary
     , hire_date
     , manager_id
  FROM employees
 WHERE manager_id IS NOT NULL
 ORDER BY hire_date ASC, salary ASC;
 
