
/*
    p292 ch12. 시퀀스 [중요]
    - 시퀀스 : PRIMARY KEY로 지정한 컬럼에 일련번호를 자동으로 부여받기 위해 사용
    - 시퀀스는 테이블내의 유일한 숫자를 자동으로 생성해 주고, 주로 기본키로 사용된다.
    (예. 게시판의 글번호, 부서테이블의 부서번호)
    - 형식
    <시퀀스 생성>
    CREATE SEQUENCE 시퀀스명
    [START WITH n]
    [INCREMENT BY n]
    [MAXVALUE n | MINVALUE n]
    [CYCLE| NOCYCLE]
    [CACHE| NOCACHE]
    
    - CURRVAL : 시퀀스의 현재값을 알아내기 위해 사용
                CURRVAL에 새로운 값이 할당되기 위해 NEXTVAL에 새로운 값을 생성해야 한다.
    - NEXTVAL : 시퀀스의 다음값을 알아내기 위해 사용
    
    <시퀀스 변경>
    ALTER SEQUENCE 시퀀스명
    -- [START WITH n] : 사용불가이며, 다른 번호에서 다시 시작하려면 이전 시퀀스를 제거하고 다시 생성한다.
        [INCREMENT BY n]
        [MAXVALUE n | MINVALUE n]
        [CYCLE| NOCYCLE]
        [CACHE| NOCACHE]
*/
DROP SEQUENCE emp_seq;
CREATE SEQUENCE emp_seq
 START WITH 1
 INCREMENT BY 1
 MAXVALUE 999
 CYCLE;
 
DROP SEQUENCE dept_seq;
CREATE SEQUENCE dept_seq
 START WITH 10
 INCREMENT BY 10
 MAXVALUE 990
 CYCLE;

SELECT * FROM user_sequences; 

-- 새로 만든 시퀀스에 nextval사용 하지 않고 바로 currval을 사용하면 오류 발생 
SELECT emp_seq.currval
  FROM dual;

-- nextval로 emp_seq 시퀀스의 새로운 값 생성
-- 현재 값을 알기 위해서 반드시 nextval을 먼저 가져와야 함
SELECT emp_seq.nextval
  FROM dual;
  
-- 1) nextval로 dept_seq 시퀀스의 새로운 값 생성하기
SELECT dept_seq.nextval
  FROM dual;

SELECT dept_seq.currval
  FROM dual; 
  
-- 시퀀스 데이터 사전에서 시퀀스 조회
SELECT * FROM user_sequences;

SELECT * FROM user_sequences
 WHERE sequence_name IN('DEPT_SEQ', 'EMP_SEQ');
 
-- 시퀀스 삭제
DROP SEQUENCE emp_seq;
DROP SEQUENCE dept_seq;

-----
-- 1. 부서 테이블 생성 dept_seq_tbl;
DROP TABLE dept_seq_tbl;
CREATE TABLE dept_seq_tbl(
    deptno      NUMBER(3)   PRIMARY KEY,
    deptname    VARCHAR2(50)NOT NULL,
    loc         VARCHAR2(50)
);

-- 2. 사원 테이블 생성 emp_seq_tbl;
DROP TABLE emp_seq_tbl;
CREATE TABLE emp_seq_tbl(
    empno       NUMBER(6)   PRIMARY KEY,
    ename       VARCHAR2(20)NOT NULL,
    hire_date   DATE        DEFAULT sysdate,
    salary      NUMBER(7,2) CHECK(salary>0),
    deptno      NUMBER(3)   REFERENCES dept_seq_tbl(deptno) ON DELETE CASCADE,
    email       VARCHAR2(60)UNIQUE
);

SELECT * FROM tab;

-- 3. 시퀀스 생성 dept_seq(10,20,...100), emp_seq(1,2,3,...,10)
DROP SEQUENCE dept_seq;
CREATE SEQUENCE dept_seq
 START WITH 10
 INCREMENT BY 10
 MAXVALUE 990
 CYCLE;

DROP SEQUENCE emp_seq;
CREATE SEQUENCE emp_seq
 START WITH 1
 INCREMENT BY 1
 MAXVALUE 999
 CYCLE;

-- 4. 각각 10건 식 INSERT 부서테이블 .. 부서번호가 seq, 사원테이블 사번이 seq
INSERT INTO dept_seq_tbl
VALUES(dept_seq.nextval, '재무', 'SAN FRANCISCO');

INSERT INTO emp_seq_tbl
VALUES(emp_seq.nextval, 'Curry', '2008/08/31', 45000, dept_seq.currval, 'sc30@gmail.com');

INSERT INTO dept_seq_tbl
VALUES(dept_seq.nextval, '총무', 'LOS ANGELES');

INSERT INTO emp_seq_tbl
VALUES(emp_seq.nextval, 'James', '2003/08/31', 42000, dept_seq.currval, 'lbj23@gmail.com');

INSERT INTO dept_seq_tbl
VALUES(dept_seq.nextval, 'IT', 'SEOUL'); 

INSERT INTO emp_seq_tbl
VALUES(emp_seq.nextval, 'Giannis', '2008/08/31', 35000, dept_seq.currval, 'ga34@gmail.com');

INSERT INTO dept_seq_tbl
VALUES(dept_seq.nextval, '제1영업', 'TOKYO'); 

INSERT INTO emp_seq_tbl
VALUES(emp_seq.nextval, 'Doncic', '2008/08/31', 25000, dept_seq.currval, 'ld77@gmail.com');

INSERT INTO dept_seq_tbl
VALUES(dept_seq.nextval, '제2영업', 'LONDON'); 

INSERT INTO emp_seq_tbl
VALUES(emp_seq.nextval, 'Lennard', '2008/08/31', 40000, dept_seq.currval, 'kl2@gmail.com');

INSERT INTO dept_seq_tbl
VALUES(dept_seq.nextval, '제1제조', 'SHANGHAI'); 

INSERT INTO emp_seq_tbl
VALUES(emp_seq.nextval, 'Durant', '2008/08/31', 40000, dept_seq.currval, 'kd07@gmail.com');

INSERT INTO dept_seq_tbl
VALUES(dept_seq.nextval, '제2제조', 'ROMA');

INSERT INTO emp_seq_tbl
VALUES(emp_seq.nextval, 'Harden', '2008/08/31', 38000, dept_seq.currval, 'jh13@gmail.com');

INSERT INTO dept_seq_tbl
VALUES(dept_seq.nextval, '제3제조', 'BOMBAY'); 

INSERT INTO emp_seq_tbl
VALUES(emp_seq.nextval, 'Tatum', '2008/08/31', 25000, dept_seq.currval, 'jt0@gmail.com');

INSERT INTO dept_seq_tbl
VALUES(dept_seq.nextval, '제4제조', 'MUNCHEN'); 

INSERT INTO emp_seq_tbl
VALUES(emp_seq.nextval, 'Westbrook', '2008/08/31', 40000, dept_seq.currval, 'rw0@gmail.com');

INSERT INTO dept_seq_tbl
VALUES(dept_seq.nextval, '개발부', 'ULSAN'); 

INSERT INTO emp_seq_tbl
VALUES(emp_seq.nextval, 'Paul', '2008/08/31', 28000, dept_seq.currval, 'cp3@gmail.com');

SELECT * FROM dept_seq_tbl;
SELECT * FROM emp_seq_tbl;

COMMIT;

--
/*
    p298 3. INDEX
    - 인덱스는 검색 속도를 향상시키기 위해서 사용
    - 기본키나 유일키는 자동으로 인덱스 생성
    - USER_COLUMNS, USER_IND_COLUMNS 데이터 사전을 통해 확인
    - 형식
        CREATE INDEX 인덱스명
        ON 테이블명(칼럼명);
    - 컬럼명은 대문자에 ''안에 들어가야 함
    
    - 인덱스를 사용조건
    1) WHERE절이나 JOIN 조건 안에서 자주 사용되는 두 개 이상의 컬럼
    2) NULL 값이 많이 포함되어 있는 컬럼
    
    - 인덱스 생성이 불필요한 경우
    1) 테이블이 작을 때
    2) 테이블이 자주 갱신될 때        
*/
SELECT * FROM tab;
-- 데이터 사전 조회
SELECT index_name
     , table_name
     , column_name
  FROM user_ind_columns     -- 데이터 사전에서 검색
 WHERE table_name IN('DEPT', 'EMP'); 
--DEPT_DEPNO_PK	DEPT	DEPTNO
--EMP_EMPNO_PK	EMP	EMPNO
--EMP_EMAIL_UQ	EMP	EMAIL

-- 인덱스 테이블 생성
CREATE TABLE dept_idx
AS 
SELECT * FROM dept;     -- 제약조건이 복사가 안됨

CREATE TABLE emp_idx
AS 
SELECT * FROM emp;      -- 제약조건이 복사가 안됨

SELECT * FROM dept_idx;
SELECT * FROM emp_idx;

SELECT index_name
     , table_name
     , column_name
  FROM user_ind_columns     -- 데이터 사전에서 검색
 WHERE table_name IN('DEPT_IDX', 'EMP_IDX'); -- PK, UNIQUE 등 제약조건이 사라졌기 때문에 INDEX가 없음

/*
    4. 인덱스의 종류
    - 고유 인덱스 : 기본키나 유일키처럼 유일한 값을 갖는 컬럼에 생성하는 인덱스이며, UNIQUE INDEX 로 사용 
    - 비고유 인덱스 : 중복된 데이터를 갖는 컬럼에 생성하는 인덱스이며, UNIQUE를 붙이면 에러 발생
    - 결합 인덱스 : 두 개 이상의 컬럼으로 인덱스를 구성
    - 함수기반 인덱스 : 수식이나 함수를 적용하여 만든 인덱스
*/ 
-- 비고유 인덱스
DROP INDEX idx_emp_ename;
CREATE INDEX idx_emp_ename
    ON emp_idx(ename);

-- 고유 인덱스
DROP INDEX idx_dept_deptno;
CREATE UNIQUE INDEX idx_dept_deptno
    ON dept_idx(deptno);

-- 결합 인덱스
DROP INDEX idx_dept_com;
CREATE INDEX idx_dept_com
    ON dept_idx(deptname, loc);

-- 함수기반 인덱스
DROP INDEX idx_emp_ansal;
CREATE INDEX idx_emp_ansal
    ON emp_idx(salary*12);
    
-- 인덱스 생성 확인
SELECT index_name
     , table_name
     , column_name
  FROM user_ind_columns     -- 데이터 사전에서 검색
 WHERE table_name IN('DEPT_IDX', 'EMP_IDX'); 
--IDX_DEPT_DEPTNO	DEPT_IDX	DEPTNO
--IDX_DEPT_COM	DEPT_IDX	DEPTNAME
--IDX_DEPT_COM	DEPT_IDX	LOC
--IDX_EMP_ENAME	EMP_IDX	ENAME 
