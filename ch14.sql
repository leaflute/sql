/*
    p336 ch14 PL/SQL
    14.1 PL/SQL
    - PL/SQL(Oracle's Procedural Language extension to SQL)
    => SQL문을 사용한 오라클의 절차적 프로그래밍 언어
    - SQL에서는 사용할 수 없는 절차적 프로그래밍 기능으로 SQL의 단점을 보완
    - PL/SQL 블록구조 : 선언부, 실행부, 예외처리부
    - 예외처리부: VALUE_ERROR
    , ROWTYPE_MISSMATCH
    , DUP_VAL_ON_INDEX
    , INVALID_CURSOR
    , LOGIN_DENIED
    , NO_DATA_FOUND
    , ZERO_DIVIDE
    - 오라클에서 화면을 출력 : 패키지명.put_line 프로시저를 이용
    - 환경변수 serveroutput을 화면출력을 위해 ON으로 변경
    - 쿼리문을 수행하기 위해서 '/'가 입력되어야 하며, PL/SQL 블록은 '/'가 있으면 종결된 것으로 간주    
*/ 
SET SERVEROUTPUT ON
BEGIN
    dbms_output.put_line('오라클에 어서오세요');
END;
/

-- CONSTANT : 상수로 지정
SET SERVEROUTPUT ON
DECLARE
    v_tax CONSTANT NUMBER(1) := 3;
BEGIN
    dbms_output.put_line('v_tax : ' || v_tax);
END;
/

-- NOT NULL => 값 설정 필수
DECLARE
    v_tax CONSTANT NUMBER(3) NOT NULL :=  10;
BEGIN
    dbms_output.put_line('v_tax : ' || v_tax);
END;
/

-- 실행결과는 [set ~ /]까지 블록설정 후 실행

/*
    03. 변수선언(DECLARE)
    1) 스칼라
    - 숫자, 문자, 날짜, BOOLEAN 4가지
    
    2) 레퍼런스 - 자바의 레퍼런스 타입(클래스, 인터페이스, 배열, ENUM)
    - %TYPE은 변수의 데이터타입을 기존컬럼에 맞추어 선언
    변수명 TABLE명.COLUMN명%type;  -- 컬럼단위로 데이터 타입을 참조
    
    - %ROWTYPE은 로우(행)전체에 대한 데이터 타입을 참조
    변수명 테이블명 테이블명%rowtype;  -- 테이블의 컬럼순서, 데이터타입, 크기까지 동일하게 사용
*/
-- 7788, scott
SET SERVEROUTPUT ON
DECLARE
    v_employee_id employees.employee_id%type;
    v_last_name employees.last_name%type;
BEGIN
    v_employee_id := 7788;
    v_last_name := 'scott';
    dbms_output.put_line('사원번호  사원명');
    dbms_output.put_line(v_employee_id || '     ' ||v_last_name);
END;
/

-- 사원번호 사원이름 급여 직무 출력하기..last_name='Nayer'(HR 계정에서 실행)
SET SERVEROUTPUT ON
DECLARE
    v_employee_id employees.employee_id%type;
    v_last_name employees.last_name%type;
    v_salary employees.salary%type;
    v_job_id employees.job_id%type;
BEGIN
    dbms_output.put_line('사원번호  사원명 급여   직무');
    
    SELECT employee_id
         , last_name
         , salary
         , job_id
    INTO v_employee_id
       , v_last_name
       , v_salary
       , v_job_id
    FROM employees
    WHERE last_name = 'Nayer';
    
    dbms_output.put_line(v_employee_id ||'  '|| v_last_name ||' '|| v_salary ||'    '|| v_job_id );
END;
/

-- EXCEPTION 예외처리
SET SERVEROUTPUT ON
DECLARE
    v_employee_id employees.employee_id%type;
    v_last_name employees.last_name%type;
    v_salary employees.salary%type;
    v_job_id employees.job_id%type;
BEGIN
    dbms_output.put_line('사원번호  사원명 급여   직무');
    
    SELECT employee_id
         , last_name
         , salary
         , job_id
    INTO v_employee_id
       , v_last_name
       , v_salary
       , v_job_id
    FROM employees
    WHERE last_name = 'Nayer';

    dbms_output.put_line(v_employee_id ||'  '|| v_last_name ||' '|| v_salary ||'    '|| v_job_id );
    
    EXCEPTION 
    WHEN VALUE_ERROR THEN
        dbms_output.put_line('예외 처리 : 수치 또는 값 오류 발생');
END;
/

-- p344
SET SERVEROUTPUT ON
DECLARE
    v_emp employees%ROWTYPE;
    v_dname VARCHAR2(20) := null;
    temp NUMBER(4) := 1;
    annsal NUMBER(7,2);
BEGIN
    SELECT * INTO v_emp
    FROM employees
    WHERE last_name LIKE '%Nayer%';
    
    IF (v_emp.commission_pct IS NULL) THEN
        v_emp.commission_pct := 0;
    END IF;
    
    IF (v_emp.department_id = 10) THEN
        v_dname := 'ACCOUNTING';
    ELSIF (v_emp.department_id = 20) THEN
        v_dname := 'RESEARCH';
    ELSIF (v_emp.department_id = 30) THEN
        v_dname := 'SALES';
    ELSIF (v_emp.department_id = 40) THEN
        v_dname := 'OPERATION';
    ELSIF (v_emp.department_id = 50) THEN
        v_dname := 'IT';
    END IF;    

    annsal := v_emp.salary * 12 + v_emp.commission_pct;
    
    dbms_output.put_line('사번    사원명     부서명     연봉');
    dbms_output.put_line('-------------------------------');
    dbms_output.put_line(v_emp.employee_id ||'   '||v_emp.last_name||'      '||v_dname||'       '||annsal);
END;
/

/*
    p346 3.2 LOOP문
*/
-- 구구단 2단출력
SET SERVEROUTPUT ON
DECLARE
    dan NUMBER := 2;
    i NUMBER := 1;
BEGIN
    LOOP
        dbms_output.put_line(
            dan||'*'||i||'='||(dan*i));
        i := i + 1;
        IF i > 9 THEN
            EXIT;
        END IF;
    END LOOP;    
END;
/

/*
    p351 4. 커서
    - 쿼리문에 의해서 반환되는 결과값들을 저장하는 메모리 공간
    - SELECT문의 수행결과가 여러개의 로우로 구해지는 경우 로우에 대한 처리를 하려면 커서를 사용
    - 커서는 쿼리문에 의해서 반환되는 결과값들을 저장하는 메모리 공간
    - CURSOR 선언 -> OPEN -> FETCH -> CLOSE
    - FETCH문은 결과셋(커서)에서 로우 단위로 데이터를 읽음
    FETCH 후에 CURSOR는 결과셋에서 다음행으로 이동
    - FETCH 문장은 현재 행에 대한 정보를 읽어와서 
    INTO 뒤에 기술한 변수에 저장한 후 출력하고, 다음 행으로 이동
    얻어진 여러개의 로우에 대한 결과값을 모두 처리하려면 반복문에 FETCH문을 사용
    
    - 형식
    DECLARE
        CURSOR 커서명
        IS
        SELECT 문장;  --  INTO 절을 포함시키지 않아야 한다.
    BEGIN
        OPEN 커서명;
        LOOP
            FETCH 커서명
            INTO 변수명;
        END LOOP;
        CLOSE 커서명;  
    END;
    / 
*/
-- 커서로부터 부서테이블의 모든 내용 조회하기(HR 계정에서 실행)
SET SERVEROUTPUT ON
DECLARE
    v_dept departments%rowtype;
    CURSOR c1
    IS
    SELECT * FROM departments;
BEGIN
    DBMS_OUTPUT.PUT_LINE('부서번호      부서명     매니저id    지역id');
    DBMS_OUTPUT.PUT_LINE('---------------------------');
    OPEN c1;
    LOOP 
        FETCH c1 INTO v_dept.department_id, v_dept.department_name, v_dept.manager_id, v_dept.location_id;
        EXIT WHEN c1%NOTFOUND;
        DBMS_OUTPUT.PUT_LINE(v_dept.department_id
                            ||'    '||v_dept.department_name
                            ||'    '||v_dept.manager_id
                            ||'    '||v_dept.location_id);
    END LOOP;
    CLOSE c1;
END;
/

SET SERVEROUTPUT ON
DECLARE
    v_dept departments%rowtype;
    -- 커서 선언 CURSOR(파라미터명 자료형1, ... 자료형n)
    CURSOR c1(p_deptno departments.department_id%type)
    IS
    SELECT * FROM departments
     WHERE department_id = p_deptno;
    
BEGIN
    DBMS_OUTPUT.PUT_LINE('부서번호      부서명     매니저id    지역id');
    DBMS_OUTPUT.PUT_LINE('---------------------------');
    -- 10번 부서 커서를 사용
    OPEN c1(10);
    LOOP 
        FETCH c1 INTO v_dept.department_id, v_dept.department_name, v_dept.manager_id, v_dept.location_id;
        EXIT WHEN c1%NOTFOUND;
        DBMS_OUTPUT.PUT_LINE(v_dept.department_id
                            ||'    '||v_dept.department_name
                            ||'    '||v_dept.manager_id
                            ||'    '||v_dept.location_id);
    END LOOP;
    CLOSE c1;
    -- 20번 부서 커서를 사용
    OPEN c1(20);
    LOOP 
        FETCH c1 INTO v_dept.department_id, v_dept.department_name, v_dept.manager_id, v_dept.location_id;
        EXIT WHEN c1%NOTFOUND;
        DBMS_OUTPUT.PUT_LINE(v_dept.department_id
                            ||'    '||v_dept.department_name
                            ||'    '||v_dept.manager_id
                            ||'    '||v_dept.location_id);
    END LOOP;
    CLOSE c1;
END;
/

-- p354 for in loop
SET SERVEROUTPUT ON
DECLARE
    v_dept departments%rowtype;
    CURSOR c1
    IS
    SELECT * FROM departments;
BEGIN
    DBMS_OUTPUT.PUT_LINE('부서번호      부서명     매니저id    지역id');
    DBMS_OUTPUT.PUT_LINE('---------------------------');
    FOR v_dept IN c1 LOOP
        EXIT WHEN c1%NOTFOUND;
        DBMS_OUTPUT.PUT_LINE(v_dept.department_id
                            ||'    '||v_dept.department_name
                            ||'    '||v_dept.manager_id
                            ||'    '||v_dept.location_id);
    END LOOP;                        
END;
/
