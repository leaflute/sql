-- 사원 테이블에서 "job_id 개수" => 중복 포함
SELECT COUNT(job_id) "job_id 개수"
  FROM employees;
-- 중복 미허용
SELECT COUNT(DISTINCT job_id) "job_id 개수"
  FROM employees;
  
/*
    p149 데이터 그룹 GROUP BY
    SELECT 컬럼명1, 컬럼명2, 그룹함수
      FROM 테이블명
     WHERE 조건(연산자)
     GROUP BY 컬럼명1, 컬럼명2
     ORDER BY 컬럼명 ASC | DESC;
       
    - 어떤 컬럼값을 기준으로 그룹함수를 적용할지 기술할 경우에는
      SELECT문의 GROUP BY 해당컬럼을 기술한다.
      
    - GROUP BY절 다음에 컬럼의 별칭은 사용할 수 없다.
    - 그룹함수가 아닌 SELECT 리스트의 모든 일반 컬럼은 GROUP BY 절에 반드시 기술해야 한다.
      그러나 반대로 GROUP BY 절에 기술된 컬럼이 반드시 SELECT 리스트에 있어야 하는건 아니다.
      단지 결과가 무의미하다.
      
    - 그룹함수는 두번까지 중첩해서 사용가능하다. 예) MAX(AVG(salalry))
      중첩그룹함수는 select list에 일반컬럼 사용불가 -> HAVING 조건절과 subquery를 사용
      예)
      SELECT department_id -- 일반컬럼 에러
           , MAX(AVG(salary))
        FROM employees
       GROUP BY department_id 
*/
-- 중첩그룹함수는 select list에 일반컬럼 사용불가 -> HAVING 조건절과 subquery사용
SELECT department_id -- 일반컬럼 에러
     , MAX(AVG(salary)) -- 중첩 그룹함수
  FROM employees
 GROUP BY department_id; 
 
-- 부서별 최대 급여
SELECT department_id
     , MAX(salary) "부서별 최대급여"
     , MIN(salary) "부서별 최소급여"
  FROM employees
 GROUP BY department_id; 

-- 사원테이블에서 직무별(job_id)로 총급여, 급여평균
SELECT job_id
     , SUM(salary) "부서별 총급여"
     , TRUNC(AVG(salary),0) "부서별 급여평균"
  FROM employees
 GROUP BY job_id;
 
-- 사원테이블에서 부서별, 직무별(job_id)로 인원수, 최대급여, 총급여, 급여평균
-- 부서, 직무는 오름차순 정렬(단, 부서는 NULL 허용안함)
SELECT department_id
     , job_id
     , COUNT(*) "사원 수"
     , MAX(salary) "최대급여"
     , SUM(salary) "총급여"
     , TRUNC(AVG(salary),0) "급여평균"
  FROM employees
 WHERE department_id IS NOT NULL
 GROUP BY department_id, job_id
 ORDER BY department_id, job_id;

/*
    P152 3. 그룹결과 제한 HAVING 매우중요
    - 표시할 그룹을 지정하여 집계정보를 기준으로 그룹결과를 제한
    - HAVING + 그룹함수 조건절
    CF) WHERE + 일반컬럼 조건절
    
    - SELECT 컬럼명N, 그룹함수
        FROM 테이블명
       WHERE 일반컬럼 조건절
       GROUP BY 컬럼명N
      HAVING 그룹함수 조건절
       ORDER BY 컬럼명 정렬순서
*/ 

-- 사원테이블에서 부서별 최대급여, 총급여(단, 총 급여가 15000 이상인 부서만), 부서코드 오름차순 정렬
-- 부서코드가 없으면 제외
SELECT MAX(salary)
     , SUM(salary)
     , department_id
  FROM employees
 WHERE department_id IS NOT NULL
 GROUP BY department_id
HAVING SUM(salary) >= 15000
 ORDER BY department_id;

-- 사원테이블에서 직무별 급여평균, 급여총액 구하기(급여 평균이 5000 이상)
-- 소수점 이하는 절삭, 직무 정렬, IT 직무 무시
SELECT job_id
     , TRUNC(AVG(salary),0)
     , SUM(salary)
  FROM employees
 WHERE job_id NOT LIKE '%IT%' 
 GROUP BY job_id
HAVING AVG(salary)>=5000
 ORDER BY job_id;