-- ch07 서브쿼리
/*
    p188 p191
    - 하나의 SELECT 문장이 절안에 포함된 또 하나의 SELECT 문장
    서브쿼리는 메인쿼리에 사용할 값을 반환
    즉, 서브쿼리에서 실행된 결과가 메인쿼리에 전달되어 최종적인 결과를 출력
    - 메인쿼리 : 서브쿼리를 포함하고 있는 쿼리문
    - 서브쿼리 : 포함된 또 하나의 쿼리문으로, 비교연산자의 오른쪽에 기술하고, 반드시 괄호안에 넣어야 한다.
                메인쿼리가 실행되기 전에 한번만 실행
    - 종류 : 단일행 서브쿼리, 다중행 서브쿼리
    - 단일행 서브쿼리 : 수행결과가 오직 하나의 행만을 반환하는 서브쿼리
            연산자 : >,>=,<,<=,=,<>
    - 다중행 서브쿼리 : 수행결과가 하나 이상의 행을 반환하는 서브쿼리
            연산자 : IN, ANY, SOME, ALL, EXISTS
*/

-- 단일행 서브쿼리
-- 'Chen'의 급여를 조회하고 그보다 많은 급여를 받는 사원의 사번, 이름, 급여를 구하시오
SELECT employee_id
     , last_name
     , salary
  FROM employees
 WHERE salary > (SELECT salary
                   FROM employees
                  WHERE last_name = 'Chen')
 ORDER BY salary;                 

-- 'CHEN'과 같은 부서에서 일하는 사원의 사번, 이름, 부서번호, 부서명 겸색
SELECT e.employee_id
     , e.last_name
     , d.department_id
     , d.department_name
  FROM departments d 
     , employees e
 WHERE d.department_id = e.department_id
   AND d.department_id  = (SELECT department_id
                             FROM employees 
                            WHERE last_name = 'Chen');
                           
-- job_title이 'Programer'인 사원과 같은 직무에서 일하는 사원의 사번, 사원명, job_id, 급여 출력
SELECT employee_id
     , last_name
     , job_id
     , salary
  FROM employees
 WHERE job_id = (SELECT job_id
                   FROM jobs 
                  WHERE job_title LIKE '%Programmer%');
                  
-- 다중행 서브쿼리
-- (1) IN : 메인쿼리의 비교조건이 서브쿼리의 결과와 하나라도 일치하면 참
-- 부서별 최소급여들(서브쿼리) 중의 하나라도 일치하는 사원의 사번, 이름, 급여(급여 정렬) 검색
SELECT MIN(salary)
     , department_id
  FROM employees
 GROUP BY department_id;
 
SELECT department_id
     , employee_id 
     , last_name
     , salary
  FROM employees 
 WHERE salary IN (SELECT MIN(salary)
                    FROM employees
                   GROUP BY department_id)             
 ORDER BY salary;                  

/*
    (2) ANY 연산자
    -- 메인 쿼리의 비교 조건이 서브 쿼리의 검색 결과와 하나 이상이 일치하면 참
    -- <ANY : 메인쿼리 결과값 < 서브쿼리 최대값
    -- >ANY : 메인쿼리 결과값 > 서브쿼리 최소값
*/
-- job_id가 'FI_ACCOUNT'인 사원의 최대급여보다 작은 사원정보 출력(사번, 이름, job_id, salary)
-- 단 job_id가 'FI_ACCOUNT'가 아닌 사원정보 출력
SELECT employee_id
     , last_name
     , job_id
     , salary
  FROM employees
 WHERE salary < (SELECT max(salary)
                   FROM employees
                  WHERE job_id = 'FI_ACCOUNT')
   AND job_id <> 'FI_ACCOUNT'
 ORDER BY salary;  

SELECT employee_id
     , last_name
     , job_id
     , salary
  FROM employees
 WHERE salary < ANY (SELECT salary
                       FROM employees
                      WHERE job_id = 'FI_ACCOUNT')
   AND job_id <> 'FI_ACCOUNT';                    

-- job_id가 'FI_ACCOUNT'인 최소급여 6900 보다 큰 급여를 받는 사원정보
SELECT employee_id
     , last_name
     , job_id
     , salary
  FROM employees
 WHERE salary > ANY (SELECT salary
                       FROM employees
                      WHERE job_id = 'FI_ACCOUNT') 
   AND job_id <> 'FI_ACCOUNT';
 /*
    (3) ALL 연산자
    -- 메인 쿼리의 비교 조건이 서브 쿼리의 검색 결과와 모든 값이 일치하면 참
    -- <ALL : 메인쿼리 결과값 < 서브쿼리 최소값
    -- >ALL : 메인쿼리 결과값 > 서브쿼리 최대값
*/
-- job_id가 'PU_CLERK'의 최소급여보다 작은 사원의 사번, 이름, job_id, salary 조회
SELECT employee_id
     , last_name
     , job_id
     , salary
  FROM employees 
 WHERE salary < ALL (SELECT salary
                       FROM employees
                      WHERE job_id = 'PU_CLERK')
   AND job_id <> 'PU_CLERK';                   

-- job_id가 'PU_CLERK'의 최대급여보다 큰 사원의 사번, 이름, job_id, salary 조회
SELECT employee_id
     , last_name
     , job_id
     , salary
  FROM employees 
 WHERE salary > ALL (SELECT salary
                       FROM employees
                      WHERE job_id = 'PU_CLERK')
   AND job_id <> 'PU_CLERK';
/*   
    (4) EXISTS <-> NOT EXISTS  
    - 메인 쿼리의 비교 조건이 서브쿼리의 검색결과 중 만족 하는 값이 하나라도 존재하면 참
    - EXISTS일 경우, 해당 결과를 모두 가져오고 NOT EXIST이면 가져오지 않음
    - 서브쿼리 값이 존재하냐 여부를 판단해, 메인 쿼리의 결과값을 반영
*/
-- 고객에게 판매한 판매액
-- 고객테이블: customer, 주문테이블: order
SELECT totalSale
  FROM orders o
 WHERE EXISTS (SELECT c.customer_id
                 FROM customer c, orders o
                WHERE c.customer_id = o.customer_id);

-- 해당 부서 정보가 존재하면 사원정보를 검색, 그렇지 않으면 검색 불가
SELECT *
  FROM employees
 WHERE EXISTS (SELECT department_name
                 FROM departments
                WHERE department_id = 100);
                
-- FROM절에 사용하는 서브쿼리(= 인라인 뷰)
-- 특정 테이블 전체 데이터가 아닌 일부 데이터를 먼저 추출 후 별칭을 주고 사용
-- 테이블 내 데이터 규모가 너무 크거나, 현재 작업에 불필요한 열이 너무 많아 일부 행과 열만 참조
-- 부서아이디가 10인 사원테이블과 부서테이블에서 정보 추출
SELECT e10.employee_id
     , e10.last_name
     , e10.department_id
     , d.department_name
     , d.location_id
  FROM (SELECT * FROM employees WHERE department_id = 10) e10
     , departments d
 WHERE e10.department_id = d.department_id;  
 
-- WITH절: 인라인뷰를 가독성있게 사용할 경우
WITH
e10 AS (SELECT * FROM employees WHERE department_id = 10),
d AS (SELECT * FROM departments)
SELECT e10.employee_id
     , e10.last_name
     , e10.department_id
     , d.department_name
     , d.location_id
FROM e10, d
WHERE e10.department_id = d.department_id;

-- (5) 스칼라(SCALA) 서브쿼리 : SELECT문에 SUBQUERY를 사용
-- 반드시 한 행과 한 컬럼만 반환하는 서브쿼리, 여러 행 반환할 시 오류발생
SELECT employee_id "사번"
     , last_name "이름"
     , job_id "직무"
     , salary "급여"
     , (SELECT TRUNC(AVG(salary)) FROM employees) "평균급여"
  FROM employees e
 WHERE employee_id = 100;