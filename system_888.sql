    -- [ 시스템 계정(System) 접속 ] ----------------------------------------------
   
   -- 2-1. 계정 생성
   -- create user <계정이름> identified by <계정암호> default tablespace users;
   -- talespace : 테이블, 뷰 및 다른 데이터베이스 객체들이 저장되는 장소
   create user scott_88 identified by tiger default tablespace users;
   
   -- 비밀번호 변경
   -- alter user <계정이름> identified by <변경 비밀번호>
   alter user scott_88 identified by tiger;
    
   -- 2-2. 사용자 권한 부여
   -- grant connect, resource, create view to <계정이름>
   grant connect, resource, create view to scott_88;
   -- grant create view to hr;

   -- 2-3. 락해제
   -- alter user <계정> account unlock;
   alter user scott_88 account unlock;
   
   -- 2-4. 계정삭제
   drop user scott_88 cascade;
   
   -- 2-5. 사용자 권한 제거
   -- revoke (권한) (ON) FROM <계정>
   revoke create view from scott_88;
   
   -- 2-6. 시스템 권한 조회
   select * from session_privs;