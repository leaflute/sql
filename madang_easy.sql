/*
02. SQL 개요
예) 김연아 고객의 전화번호를 찾으시오.
*/

-- 3.1.1 SELECT/FROM
-- 3-1 모든 도서의 이름과 가격을 검색하시오.
SELECT bookname
     , price   
  FROM book;
 
-- (3-1 변형) 모든 도서의 가격과 이름을 검색하시오.
SELECT price
     , bookname
  FROM book;

-- 3-2 모든 도서의 도서번호, 도서이름, 출판사, 가격을 검색하시오.
SELECT bookid
     , bookname
     , publisher
     , price
  FROM book;

-- 3-3 도서 테이블에 있는 모든 출판사를 검색하시오.
SELECT publisher
  FROM book;

-- 3.1.2 WHERE 조건
-- 3-4 가격이 20,000원 미만인 도서를 검색하시오.
SELECT *
  FROM book
 WHERE price < 20000;

-- 3-5 가격이 10,000원 이상 20,000 이하인 도서를 검색하시오.
SELECT *
  FROM book
 WHERE price BETWEEN 10000 AND 20000;

-- 3-6 출판사가 '굿스포츠'혹은 '대한미디어'인 도서를 검색하시오.
SELECT *
  FROM book
 WHERE publisher = '굿스포츠'
    OR publisher = '대한미디어';

-- 3-7 '축구의 역사'를 출간한 출판사를 검색하시오.
SELECT publisher
     , bookname
  FROM book
 WHERE bookname = '축구의 역사';
 
-- 3-8 도서이름에 '축구'가 포함한 출판사를 검색하시오.
SELECT publisher
     , bookname
  FROM book
 WHERE bookname LIKE '%축구%'; 

-- 3-9 도서이름의 왼쪽 두 번째 위치에 '구'라는 문자열을 갖는 도서를 검색하시오.
SELECT bookname
  FROM book
 WHERE bookname LIKE '_구%';
 
-- 3-10 축구에 관한 도서 중 가격이 20,000원 이상인 도서를 검색하시오.
SELECT bookname
     , price
  FROM book
 WHERE price >= 20000
   AND bookname LIKE '축구%';
                  
-- 3-11 출판사가 '굿스포츠' 혹은 '대한미디어'인 도서를 검색하시오.
SELECT bookname
  FROM book
 WHERE publisher = '굿스포츠'
    OR publisher = '대한미디어';

-- 3-12 도서를 이름순으로 검색하시오
SELECT *
  FROM book
 ORDER BY bookname ASC;
 
-- 3-13 도서를 가격순으로 검색하고, 가격이 같으면 이름순으로 검색하시오
SELECT *
  FROM book
 ORDER BY price ASC, bookname ASC;
 
-- 3-14 도서를 가격의 내림차순으로 검색하시오. 만약 가격이 같다면 출판사의 오름차순으로 검색한다.
SELECT *
  FROM book
 ORDER BY price DESC, publisher ASC;
  
-- 3-15 고객이 주문한 도서의 총 판매액을 구하시오.
SELECT SUM(saleprice)
  FROM orders;

-- 3-16 2번 김연아 고객이 주문한 도서의 총 판매액을 구하시오.
SELECT SUM(saleprice)
  FROM orders
 WHERE custid = 2;

-- 3-17 고객이 주문한 도서의 총판매액, 평균값, 최저가, 최고가를 구하시오
SELECT SUM(saleprice) "총판매액"
     , AVG(saleprice) "평균값"
     , MIN(saleprice) "최저가"
     , MAX(saleprice) "최고가"
  FROM orders;

-- 3-18 마당서점의 도서 판매 건수를 구하시오.
SELECT COUNT(*) "도서 판매 건수"
  FROM orders;

-- 3.2.2 GROUP BY
-- 3-19 고객별로 주문한 도서의 총 수량과 총 판매액을 구하시오.
SELECT custid
     , COUNT(*) "주문 수량"
     , SUM(saleprice) "총 구매액"
  FROM orders
 GROUP BY custid;

--3-20 가격이 8,000원 이상인 도서를 구매한 고객에 대하여 고객별 주문 도서의 총 수량을 구하시오. 단, 두권 이상 구매한 고객만 구한다.
SELECT custid
     , COUNT(*)
  FROM orders 
 WHERE saleprice >= 8000
 GROUP BY custid 
HAVING COUNT(*) >= 2;

--3-21 고객과 고객의 주문에 관한 데이터를 모두 보이시오.
SELECT *
  FROM customer c, orders o
 WHERE o.custid = c.custid;
 
--3-22 고객과 고객의 주문에 관한 데이터를 고객번호 순으로 정렬하여 보이시오.
SELECT *
  FROM customer c, orders o
 WHERE o.custid = c.custid
 ORDER BY c.custid;
 
--3-23 고객의 이름과 고객이 주문한 도서의 판매가격을 검색하시오.
SELECT c.name
     , o.saleprice
  FROM customer c, orders o
 WHERE o.custid = c.custid;

--3-24 고객별로 주문한 모든 도서의 총 판매액을 구하고, 고객별로 정렬하시오.
SELECT o.custid
     , SUM(o.saleprice) "고객별 도서 총판액"
  FROM customer c, orders o
 WHERE o.custid = c.custid
 GROUP BY o.custid;

--3-25 고객의 이름과 고객이 주문한 도서의 이름을 구하시오.
SELECT c.name
     , b.bookname
  FROM customer c, orders o, book b
 WHERE o.custid = c.custid
   AND b.bookid = o.bookid;

--3-26 가격이 20,000원인 도서를 주문한 고객의 이름과 도서의 이름을 구하시오.
SELECT c.name
     , b.bookname
     , b.price
  FROM customer c
     , orders o
     , book b
 WHERE o.custid = c.custid
   AND b.bookid = o.bookid
   AND b.price >= 20000;

--3-27 도서를 구매하지 않은 고객도 포함하여 고객의 이름과 고객이 주문한 도서의 판매가격을 구하시오.
SELECT c.name
     , o.saleprice
  FROM customer c
     , orders o
 WHERE o.custid(+) = c.custid;

SELECT c.name
     , o.saleprice
  FROM customer c LEFT OUTER JOIN orders o
    ON o.custid = c.custid;
    
SELECT c.name
     , o.saleprice
  FROM orders o RIGHT OUTER JOIN customer c
    ON o.custid = c.custid;    

-- 3.3.2 부속질의
-- 3-28 가장 비싼 도서의 이름을 보이시오.
SELECT bookname
     , price
  FROM book
 WHERE price = (SELECT MAX(price) 
                  FROM book);
                  
-- 3-29 도서를 구매한 적이 있는 고객의 이름을 검색하시오.
SELECT DISTINCT c.name
  FROM customer c, orders o
 WHERE o.custid = c.custid
   AND o.custid IN (SELECT custid
                      FROM orders); 

-- 3-30 대한미디어에서 출판한 도서를 구매한 고객의 이름을 보이시오.
SELECT c.name
     , b.bookname
     , b.publisher
  FROM customer c, book b, orders o
 WHERE c.custid = o.custid
   AND b.bookid = o.bookid
   AND b.publisher IN (SELECT publisher
                         FROM book
                        WHERE publisher = '대한미디어');

-- 3-31 출판사별로 출판사의 평균 도서 가격보다 비싼 도서를 구하시오.
SELECT AVG(price)
  FROM book     
 GROUP BY publisher;

SELECT b1.bookname
     , b1.price
  FROM book b1
 WHERE b1.price > (SELECT AVG(b2.price)
                     FROM book b2
                    WHERE b1.publisher = b2.publisher); 

-- 3-32 도서를 주문하지 않은 고객의 이름을 보이시오.
SELECT DISTINCT c.name
  FROM customer c, orders o
 WHERE o.custid(+) = c.custid 
 MINUS
SELECT DISTINCT c.name
  FROM customer c, orders o
 WHERE c.custid = o.custid;
 
-- 3-33 주문이 있는 고객의 이름과 주소를 보이시오.
SELECT DISTINCT c.name
  FROM customer c, orders o
 WHERE c.custid = o.custid;

-- 4.1 CREATE 문
-- 3-34 다음과 같은 속성을 가진 NewBook 테이블을 생성하시오. 정수형은 NUMBER를, 문자형은 가변형 문자타입인 VARCHAR2를 사용한다.
-- bookid	NUMBER,
-- bookname  VARCHAR2(20),
-- publisher  VARCHAR2(20),
-- price      NUMBER);
SELECT * FROM tab;
DROP TABLE newbook;
CREATE TABLE newbook (
     bookid	    NUMBER,
     bookname   VARCHAR2(20),
     publisher  VARCHAR2(20),
     price      NUMBER          
);

--3-35 다음과 같은 속성을 가진 NewCustomer 테이블을 생성하시오.
--custid(고객번호) NUMBER, 기본키
--name(이름)-VARCHAR2(40)
--address(주소)-VARCHAR2(40)
--phone(전화번호)-VARCHAR2(30)
CREATE TABLE newcustomer(
    custid  NUMBER PRIMARY KEY,
    name    VARCHAR2(40) NOT NULL,
    address VARCHAR2(40) NOT NULL,
    phone   VARCHAR2(30) UNIQUE
);

--3-36 다음과 같은 속성을 가진 NewOrders 테이블을 생성하시오.
--orderid(주문번호)- NUMBER, 기본키
--custid(고객번호)- NUMBER, NOT NULL 제약조건, 외래키(NewCustomer.custid, 인쇄삭제)
--bookid(도서번호)- NUMBER, NOT NULL 제약조건
--saleprice(판매가격)-NUMBER
--orderdate(판매일자)-DATE
DROP TABLE neworders;
CREATE TABLE neworders(
    orderid     NUMBER      PRIMARY KEY, 
    custid      NUMBER      NOT NULL, 
    bookid      NUMBER      NOT NULL,
    saleprice   NUMBER      , 
    orderdate   DATE        DEFAULT sysdate,
    CONSTRAINT neworders_custid_fk FOREIGN KEY(custid) REFERENCES newcustomer(custid)
        ON DELETE CASCADE,
    CONSTRAINT neworders_saleprice_min CHECK(saleprice > 0)
);

--4.2 ALTER문
DESC newbook;
--3-37 NewBook 테이블에 VARCHAR2(13)의 자료형을 가진 isbn 속성을 추가하시오.
ALTER TABLE newbook
ADD isbn VARCHAR2(13);

--3-38 NewBook 테이블의 isbn 속성의 데이터 타입을 NUMBER형으로 변경하시오.
ALTER TABLE newbook
MODIFY isbn NUMBER;

--3-39 NewBook 테이블의 isbn 속성을 삭제하시오.
ALTER TABLE newbook
DROP COLUMN isbn;

--3-40 NewBook 테이블의 bookid 속성에 NOT NULL 제약조건을 적용하시오.
ALTER TABLE newbook
MODIFY bookid CONSTRAINT newbook_bookid_nn NOT NULL;

--3-41 NewBook 테이블의 bookid 속성을 기본키로 변경하시오.
ALTER TABLE newbook
DROP CONSTRAINT newbook_bookid_nn;

ALTER TABLE newbook
MODIFY bookid CONSTRAINT newbook_bookid_pk PRIMARY KEY;

--4.3 DROP 문
--3-42 NewBook 테이블을 삭제하시오.
DROP TABLE newbook;

--3-43 NewCustomer 테이블을 삭제하시오. 만약 삭제가 거절된다면 원인을 파악하고 관련된 테이블을 같이 삭제하시오(NewOrders 테이블이 NewCustomer를 참조하고 있음).
DROP TABLE neworders;
DROP TABLE newcustomer;