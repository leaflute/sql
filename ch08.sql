-- ch08
/*
    p206 테이블 생성, 수정, 제거 DDL(데이터 정의어 : Data Definition Language)
    1-1. 데이터 구조를 만드는 CREAD TABLE문
    - 형식 : CREATE TABLE [schema.]table(
            컬럼 데이터타입 [DEFAULT 표현식] [제약조건]
    - schema : 소유자의 이름(사용자 계정)      
*/

-- menu 테이블 생성
DROP TABLE menu_tbl;
CREATE TABLE menu_tbl(
    food_code   CHAR(5)   PRIMARY KEY, -- NOT NULL, UNIQUE
    restaurant_name     VARCHAR2(50) NOT NULL,
    food_kind   CHAR(2)     NOT NULL,
    food_name   VARCHAR2(1000)  NOT NULL,
    food_price  NUMBER(8)   NOT NULL,
    starpoint   NUMBER(1)   DEFAULT 0 -- 1~5
);

DESC menu_tbl;  -- 구조 확인
SELECT * FROM tab; -- 계정 내 테이블 목록

-- 1. 테이블에 내용을 추가하는 INSERT문
/*
    - 형식: INSERT INTO table_name(컬럼1, 컬럼2, ... 컬럼n)
            VALUES(값1, 값2, .... 값n)
    - 개수, 순서, 데이터타입 주의
    - COMMIT; 영구저장
*/

-- 컬럼명 생량시 모든 컬럼에 대해서 값을 모두 지정해야 함
-- 테이블을 생성할 때 정의한 컬럼한 순서와 동일한 순서대로 VALUES 이하의 값이 입력되기 때문

INSERT INTO menu_tbl(food_code, restaurant_name, food_kind, food_name, food_price, starpoint)
VALUES('1', '느그집 스시', 'JP', '모듬초밥', 15000, 5); -- 중복된 PK값으로 인해 오류

INSERT INTO menu_tbl(food_code, restaurant_name, food_kind, food_name, food_price, starpoint)
VALUES('느그집 스시', 'JP', '모듬초밥', 15000, 5); -- 값 입력 누락 오류

INSERT INTO menu_tbl(food_code, restaurant_name, food_kind, food_name, food_price, starpoint)
VALUES('', '느그집 스시', 'JP', '모듬초밥', 15000, 5); -- NULL값 입력 불가 오류

INSERT INTO menu_tbl(food_code, restaurant_name, food_kind, food_name, food_price, starpoint)
VALUES('0', '느그집 레스토랑', 'IT', '볼로냐 스파게티', 7000, 3);

INSERT INTO menu_tbl(food_code, restaurant_name, food_kind, food_name, food_price, starpoint)
VALUES('1', '느그집 스시', 'JP', '모듬초밥', 15000, 5);

INSERT INTO menu_tbl(food_code, restaurant_name, food_kind, food_name, food_price, starpoint)
VALUES('2', '버거킹', 'US', '와퍼세트', 7400, 2);

INSERT INTO menu_tbl(food_code, restaurant_name, food_kind, food_name, food_price, starpoint)
VALUES('3', '파파존스', 'US', '스파이시 치킨렌치F', 34900, 4);

COMMIT; -- 영구저장(INSERT,UPDATE,DELETE 문장 실행 후 마지막에 반드시 실행)

SELECT * FROM menu_tbl;

DELETE menu_tbl;

/*
    p207 1-2. subquery문으로 테이블의 구조와 데이터 복사
    CREATE TABLE 테이블명(컬럼명1,컬럼명2,....,컬럼명n)
    AS subquery;
    - NOT NULL 조건만 복사하고, PK, FK같은 무결성 제약조건은 복사되지 않음
*/
CREATE TABLE menu_tbl_copy -- CREATE는 자동으로 COMMIT됨
AS
SELECT * FROM menu_tbl;

DESC menu_tbl_copy; 
SELECT * FROM menu_tbl_copy;

INSERT INTO menu_tbl_copy(food_code, restaurant_name, food_kind, food_name, food_price, starpoint)
VALUES('3', '느그집 레스토랑', 'IT', '볼로냐 스파게티', 7000, 3);

---
/*
    p209 1-3. 서브쿼리문으로 테이블의 구조만 복사하기
    CREATE TABLE 테이블명(컬럼명1,...)
        AS subquery
     WHERE 0=1;
    - 0=1은 항상 거짓이므로 데이터 조회 불가
*/

-- 서브쿼리문으로 테이블의 구조만 복사하기
DROP TABLE menu_tbl_copy2;
CREATE TABLE menu_tbl_copy2
AS (SELECT * FROM menu_tbl WHERE 0=1);

DESC menu_tbl_copy2;
SELECT * FROM menu_tbl_copy2;


/*
    2-1. 테이블 구조를 변경하는 ALTER TABLE문
    1) 컬럼추가 : ADD
    - ALTER TABLE문을 사용하여 컬럼을 추가, 수정, 삭제 가능
    - ALTER TABLE -- ADD구문을 사용하여 새로운 컬럼을 추가한다. 컬럼 맨 끝에 추가.
    - 형식
    ALTER TABLE 테이블명
    ADD (컬럼명 데이터타입 DEFAULT expr, ...컬럼명n 데이터타입 DEFAULT expr)
*/
-- menu_tbl_copy 테이블에 영업일 컬럼 추가(start_date DATE .. sysdate)
ALTER TABLE menu_tbl_copy
ADD (start_date DATE DEFAULT sysdate);

DESC menu_tbl_copy;
SELECT * FROM menu_tbl_copy;

ALTER TABLE menu_tbl_copy
ADD (location VARCHAR2(30) DEFAULT null);

/*
    p211 2-1. 테이블 구조를 변경하는 ALTER TABLE문
    1) 컬럼변경 : MODIFY
    - ALTER TABLE .. MODIFY구문을 사용해 기본 컬럼의 데이터타입, 크기, 기본값을 변경
    데이터는 유지
    - 형식
    ALTER TABLE 테이블명
    MODIFY (컬럼명, 데이터타입 DEFAULT expr, ...컬럼명n 데이터타입 DEFAULT expr)
*/
-- food_price를 10자리로 바꿀것
ALTER TABLE menu_tbl_copy
MODIFY food_price NUMBER(10);

DESC menu_tbl_copy;
SELECT * FROM menu_tbl_copy;

/* 
    P211 2-2. 테이블 구조를 변경하는 ALTER TABLE문
    1) 컬럼변경 : MODIFY
    -- ALTER TABLE .. MODIFY 구문을 사용하여 기존 컬럼의 데이터타입, 크기, 기본값을 변경한다.
    데이터는 그대로 유지
    
    [형식]
    ALTER TABLE 테이블명
    MODIFY([컬럼명 데이터타입 DEFAULT expr
         ,컬럼명 데이터타입 ....]);
*/
-- food_price NUMBER(10)으로 변경
ALTER TABLE menu_tbl_copy
MODIFY food_price NUMBER(10);

/* 
    P212 2-3. 테이블 구조를 변경하는 ALTER TABLE문
    1) 컬럼제거 : DROP COL UMN
    - ALTER TABLE .. DROP COLUMN 구문을 사용하여 특정컬럼과 컬럼내의 데이터를 제거 가능
    - 2개 이상의 컬럼이 존재하는 테이블 내에서만 가능하며, 한번에 하나의 컬럼만 삭제 가능
    - 삭제된 컬럼은 복구불가(자동 커밋되므로 ROLLBACK(취소) 불가)
    - 형식
    ALTER TABLE 테이블명
    DROP COLUMN 컬럼명;
*/
-- menu_tbl_copy 테이블에 location 컬럼 삭제
ALTER TABLE menu_tbl_copy
DROP COLUMN location;

-- 구조확인
DESC menu_tbl_copy;

-- 결과조회
SELECT * FROM menu_tbl_copy;

/* 
    p214 3. 테이블명을 변경하는 RENAME문
    -형식
    RENAME old_TABLE TO new_TABLE;
*/
RENAME menu_tbl_copy2 TO menu_tbl_copy2_new;


/*
    p215 4. 테이블 구조를 제거하는 DROP TABLE문
    - DROP TABLE문을 사용하여 기존의 테이블과 데이터를 모두 제거 가능
    - 삭제할 테이블의 기본 키나 고유 키를 다른 테이블에서 참조 중인 경우 삭제가 불가
    그럴 때는 참조 중인 자식테이블을 먼저 제거해야 함
*/
DROP TABLE menu_tbl_copy2_new2;
SELECT * FROM tab;

/*
    p216. 5.테이블 모든 데이터를 제거하는 TRUNCATE TABLE문
    - TRUNCATE TABLE 문을 사용하여 테이블의 구조를 유지한 채 데이터와 할당된 공간을 제거, 롤백불가
    - 형식
    TRUNCATE TABLE 테이블명;
*/
CREATE TABLE menu_tbl_truncate(
    food_code           CHAR(5)         PRIMARY KEY, -- NOT NULL, UNIQUE
    restaurant_name     VARCHAR2(50)    NOT NULL,
    food_kind           CHAR(2)         NOT NULL,
    food_name           VARCHAR2(1000)  NOT NULL,
    food_price          NUMBER(8)       NOT NULL,
    starpoint           NUMBER(1)       DEFAULT 0 -- 1~5
);
INSERT INTO menu_tbl_truncate(food_code, restaurant_name, food_kind, food_name, food_price, starpoint)
VALUES('0', '느그집 레스토랑', 'IT', '볼로냐 스파게티', 7000, 3);

TRUNCATE TABLE menu_tbl_truncate;
DESC menu_tbl_truncate;

SELECT * FROM menu_tbl_truncate;

-- 다른테이블에서 데이터 폭사 (menu_tbl -> menu_tbl_truncate).. 서브쿼리로 다중행 입력
INSERT INTO menu_tbl_truncate
SELECT * FROM menu_tbl;

-- 데이터 삭제
DELETE FROM menu_tbl_truncate;
COMMIT;
ROLLBACK; -- 데이터이나 삽입 삭제를 커밋 이후 시점으로 되돌림, 커밋이 세이브 포인트라고 생각하면 됨

/*
    ROLLBACK과 
    DROP(테이블 삭제), TRUNCATE(테이블 초기화)는 자동 COMMIT
    DELETE(데이터 삭제) COMMIT전에 ROLLBACK을 하면 이전 COMMIT 시점으로 돌아감
*/
----
/*
    6. 데이터 사전
    - 데이터 사전은 사용자와 데이터베이스 자원의 효율적관리를 위한 다양한 정보를 저장하는 시스템 테이블의 집합이다.
    - 사용자가 테이블을 생성하거나 사용자를 변경하는 등의 작업을 할 때 데이터베이스 서버에 의해
      자동으로 갱신되는 테이블이다.
    - 사용자는 데이터 사전의 내용을 직접 수정하거나 삭제할 수 없고, 읽기 전용 뷰 형태로 사용자에게 제공된다.
    -[ 접두어 ] 의미
     USER_  : 자신의 계정이 소유한 객체 등에 관한 정보 조회
     ALL_ : 자신의 계정이 소유했거나 권한을 부여받은 객체 등에 관한 정보 조회
     DBA_ : 데이터베이스 관리자만 접근 가능한 객체 등의 정보 조회
*/
-- 6-1.데이터 사전 - USER_데이터 사전
-- USER_SEQUENCES : 사용자가 소유한 시퀀스의 정보
-- USER_INDEXES : 사용자가 소유한 인덱스의 정보
-- USER_VIEWS : 사용자가 소유한 뷰의 정보

-- scot_88에서 실행
-- user_데이터사전: 개인이 자신의 계정에서 만든 테이블 모 ㄱ록 조회
SELECT table_name
  FROM user_tables;

-- system 계정에서 실행 
-- 6-2. 데이터사전: DBA데이터사전
SELECT owner, table_name
  FROM dba_tables;