/*
    ch16 고-급 SQL
    1. 집합 
    - SELECT 리스트의 컬럼 개수, 컬럼 자료형이 순서대로 일치해야 사용 
    (데이터가 없을 때 대응되는 컬럼에 NULL을 사용)
    - ORDER BY는 전체 문장의 맨 끝에 와야 함
    1) UNION 연산자(합집합) : 중복 제거
    2) UNION ALL 연산자(합집합) : 중복행을 제거하지 않음
    3) INTERSECT 연산자(교집합) 
    4) MINUS 연산자(차집합) 
*/
-- HR계정
-- 1) UNION 연산자(합집합) : 중복 제거
SELECT employee_id
     , last_name
     , salary
     , department_id
  FROM employees
 WHERE department_id = 10
 UNION
SELECT employee_id
     , last_name
     , salary
     , department_id
  FROM employees
 WHERE department_id = 20; 

-- 2) UNION ALL 연산자(합집합) : 중복행을 제거하지 않음 
 SELECT employee_id
     , last_name
     , salary
     , department_id
  FROM employees
 WHERE department_id = 10
 UNION ALL
SELECT employee_id
     , last_name
     , salary
     , department_id
  FROM employees
 WHERE department_id = 10; 

-- 모든 사원의 현재 및 이전 직무사항을 조회하되, 각 사원정보는 한 번만 조회 
SELECT employee_id
     , job_id
  FROM employees
 UNION
SELECT employee_id
     , job_id
  FROM job_history;
--
-- 3) INTERSECT 연산자(교집합) 
SELECT employee_id
     , last_name
     , salary
     , department_id
  FROM employees
INTERSECT
SELECT employee_id
     , last_name
     , salary
     , department_id
  FROM employees
 WHERE department_id = 10;   
 
SELECT employee_id
     , job_id
  FROM employees
INTERSECT
SELECT employee_id
     , job_id
  FROM job_history; 
  
-- 4) MINUS 연산자(차집합)   
SELECT employee_id
     , last_name
     , salary
     , department_id
  FROM employees
 MINUS
SELECT employee_id
     , last_name
     , salary
     , department_id
  FROM employees
 WHERE department_id = 10
 ORDER BY department_id;

/*
    2. VIEW를 이용한 순위 구하기
    - ROWNUM : 행 번호
*/
-- HR계정에서 실행
-- 입사일이 빠른 순서로 5명 출력(인라인 뷰)
SELECT ROWNUM "순위"
     , employee_id
     , last_name
     , hire_date
     , salary
  FROM (
    SELECT ROWNUM, employee_id
         , last_name
         , hire_date
         , salary
      FROM employees
     ORDER BY hire_date ASC)
 WHERE ROWNUM <= 5
   AND hire_date IS NOT NULL;
   
-- 급여가 적은 사람부터 10명 출력(인라인 뷰)
SELECT ROWNUM "순위"
     , employee_id
     , last_name
     , hire_date
     , salary
  FROM (
    SELECT ROWNUM, employee_id
         , last_name
         , hire_date
         , salary
      FROM employees
     ORDER BY salary ASC)
 WHERE ROWNUM <= 10
   AND hire_date IS NOT NULL;

SELECT *
  FROM employees
 WHERE salary = 2500;
 
/*
    3. 분석함수
    - 테이블에 있는 데이터를 특정용도로 분석하여 결과를 반환하는 함수
    - PARTITION BY : 계산 대상을 그룹으로 정함
    - OVER : 분석함수임을 나타내는 키워드
    - 분석함수는 그룹함수와 그룹단위로 값을 계산한다는 점에서 유사하나
    그룹이 아닌 결과 SET의 각 행마다 집계결과를 표시
    그룹별 계산결과를 각 행마다 표시
*/
-- HR계정에서 실행
-- 각각의 동일부서의 급여합계
SELECT department_id
     , employee_id
     , salary
     , SUM(salary) OVER(PARTITION BY department_id) "부서별 급여합계"
  FROM employees
 ORDER BY department_id; 
 
/*
    4. 순위함수
    - RANK() 함수 : 순위를 부여해 동일 순위 처리가 가능
    (중복 순위 다음 순서는 건너뜀 - 1,2,2,4 ...)
    - DENSE RANK() 함수 : RANK 함수와 같은 역할을 하나, 동일 등수가 순위에 영향을 미치지 않음 
    (중복 순위 다음 순서를 건너뛰지 않음 - 1,2,2,3 ...)
    - ROW NUMBER() 함수 : 특정 순위 일련번호를 제공하는 함수로 동일 순서 처리가 불가
    (중복 순위 없이 유일값 사용 - 1,2,3,4 ...)
    
    - ORDER BY 필수
    - NTILE(분류) 함수는 쿼리의 결과를 n개의 그룹으로 분류하는 기능
    - NTILE(분류숫자) 함수는 지정한 븐류숫자 만큼의 그룹으로 분류
*/
SELECT department_id
     , employee_id
     , salary
     , RANK() OVER(ORDER BY salary DESC) AS RANK
  FROM employees;

-- 순위함수 비교
SELECT department_id
     , employee_id
     , salary
     , RANK() OVER(ORDER BY salary DESC) AS rank
     , DENSE_RANK() OVER(ORDER BY salary DESC) AS d_rank
     , ROW_NUMBER() OVER(ORDER BY salary DESC) AS r_number
  FROM employees;
  
-- 순위함수 분류
SELECT employee_id
     , NTILE(2) OVER(ORDER BY employee_id) grp2 -- 전체를 2등분
     , NTILE(3) OVER(ORDER BY employee_id) grp3 -- 전체를 3등분
     , NTILE(5) OVER(ORDER BY employee_id) grp5 -- 전체를 5등분
FROM employees;

/*
    5. 윈도우 함수
    - 분석함수 중에 윈도우절(WINDOWING)을 사용하는 함수
    - WINDOW절을 사용하게 되면 PARTITION BY 절에 명시된 그룹을 좀 더 세부적으로 그룹핑할 수 있음
    - 윈도우절은 분석함수 중에서 일부(AVG, SUM, MAX, MIN, COUNT)만 사용
    - ROWS : 물리적인 ROW 단위로 행집합을 지정
    - RANGE : 논리적인 상대번지로 행집합을 지정
*/
-- ROWS 예제
-- 부서별로 이전 ROW의 급여와 현재 ROW의 급여합계를 출력
-- ROWS 2 PRECEDING -> 현재 + 이전 + 그 이전
SELECT employee_id
     , last_name
     , department_id                                         
     , salary
     , SUM(salary) OVER(PARTITION BY department_id
                        ORDER BY employee_id
                        ROWS 2 PRECEDING) pre_sum
  FROM employees;

-- RANGE 예제 
-- 영업정보 시스템에서 분석 화면에 전월비교, 전년비교, 분기별 합계, 분기별 평균
-- PRECEDING 이전, FOLLOWING 이후
WITH test AS
(
    SELECT '202101' yyyymm, 100 amt FROM dual
     UNION ALL SELECT '202102' YYYYMM, 200 amt FROM dual
     UNION ALL SELECT '202103' YYYYMM, 300 amt FROM dual
     UNION ALL SELECT '202104' YYYYMM, 400 amt FROM dual
     UNION ALL SELECT '202105' YYYYMM, 500 amt FROM dual
     UNION ALL SELECT '202106' YYYYMM, 600 amt FROM dual
     UNION ALL SELECT '202108' YYYYMM, 800 amt FROM dual
     UNION ALL SELECT '202109' YYYYMM, 900 amt FROM dual
     UNION ALL SELECT '202110' YYYYMM, 1000 amt FROM dual    
     UNION ALL SELECT '202111' YYYYMM, 1100 amt FROM dual
     UNION ALL SELECT '202112' YYYYMM, 1200 amt FROM dual
)
SELECT yyyymm
     , amt
     , SUM(amt) OVER(ORDER BY TO_DATE(yyyymm,'yyyymm')
                RANGE BETWEEN INTERVAL '3' MONTH PRECEDING
                          AND INTERVAL '1' MONTH PRECEDING) amt_pre3                    
     , SUM(amt) OVER(ORDER BY TO_DATE(yyyymm,'yyyymm')
                RANGE BETWEEN INTERVAL '1' MONTH FOLLOWING
                          AND INTERVAL '3' MONTH FOLLOWING) amt_post3 
  FROM test;
  
/*
    6. ROLLUP
    - GROUP BY 구문의 결과에 소계 및 합계 정보를 추가로 출력하는 함수
    - ROLLUP : 단계별 소계
*/
-- ROLLUP 예제
-- 1. 부서별 급여합계
SELECT department_id
     , SUM(salary)
     , ROUND(AVG(salary),0) "평균"
     , MAX(salary) "최대급여"
     , COUNT(*) "사원 수"
  FROM employees
 GROUP BY ROLLUP(department_id);
-- 마지막 전체 부서 총급여합계

-- 2. 부서별 급여합계와 직무별 전체 합계(총계)를 조회
SELECT DECODE(department_id, NULL, '총합계', department_id) "부서" 
     , DECODE(job_id, NULL, '부서별 합계', job_id) "직무"
     , SUM(salary) "합계"
     , ROUND(AVG(salary),0) "평균"
     , MAX(salary) "최대급여"
     , COUNT(*) "사원 수"
  FROM employees
 WHERE department_id IS NOT NULL 
 GROUP BY ROLLUP(department_id, job_id);
 
/*
    7. CUBE
    - 지정한 모든 열에서 가능한 조합의 결과를 모두 출력
*/
-- CUBE 함수를 적용한 그룹화
-- 부서별, 직무별 급여합계, 총 합계 조회
SELECT DECODE(department_id, NULL, '총합계', department_id) "부서" 
     , DECODE(job_id, NULL, '부서별 합계', job_id) "직무"
     , SUM(salary) "합계"
     , ROUND(AVG(salary),0) "평균"
     , MAX(salary) "최대급여"
     , COUNT(*) "사원 수"
  FROM employees
 WHERE department_id IS NOT NULL 
 GROUP BY CUBE(department_id, job_id)
 ORDER BY department_id, job_id;

/*
    8. GROUPING SETS
    - 부서별 그룹결과 출력, job별로 그룹결과 출력
*/
SELECT DECODE(department_id, NULL, '직무별 합계', department_id) "부서" 
     , DECODE(job_id, NULL, '부서별 합계', job_id) "직무"
     , SUM(salary) "합계"
     , ROUND(AVG(salary),0) "평균"
     , MAX(salary) "최대급여"
     , COUNT(*) "사원 수"
  FROM employees
 WHERE department_id IS NOT NULL 
 GROUP BY GROUPING SETS(department_id, job_id)
 ORDER BY department_id, job_id;
 